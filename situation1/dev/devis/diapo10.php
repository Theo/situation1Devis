<?php
session_start();
include('../init/database.php');

$id = $_GET['id'];
$connexion->query("SET NAMES UTF8");
$query = "SELECT telephone, nom, prenom, mail
FROM utilisateur
INNER JOIN devis ON utilisateur.id = devis.user
WHERE devis.id =".$id;
$req = $connexion->query($query);
while ($ligne = $req->fetch(PDO::FETCH_ASSOC)) 
{
    $nom=$ligne['nom'];
    $prenom=$ligne['prenom'];
    $telephone=$ligne['telephone'];
    $mail=$ligne['mail'];
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Devis en ligne</title>
        <style>
            span.carte {
                position:absolute; 
                text-align:right;
                float: right;
                top:50%;
                text-align: center;
            }
            span.nom {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:415px;
        left:615px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15pt;
            }
            span.prenom {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:435px;
        left:615px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15pt;
            }
            span.mail {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:455px;
        left:615px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15pt;
            }
            span.tel {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:475px;
        left:615px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15pt;
            }
        </style>
    </head>
    <body style='height: 100%;width:100%;margin:0;'>
        <!-- page content -->
        <div style="width:1000px;">
            <div class="">

                <div class="clearfix"></div>
            <div class="row">
                <center>
                    <img src="../images/mission/diapo11.PNG" width="97%">
                </center>
                <span class="nom"><?php echo 'Nom : '.$nom;?></span>
                <span class="prenom"><?php echo 'Prénom : '.$prenom;?></span>
                <span class="mail"><?php if ($mail==''){echo 'E-mail : Non renseigné';}else{echo 'E-mail : '.$mail;}?></span>
                <span class="tel"><?php if ($telephone==''){echo 'Téléphone : Non renseigné';}else{ echo 'Téléphone : '.$telephone;}?></span>
            </div>
            </div>
        </div>
    </body>
</html>