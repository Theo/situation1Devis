<?php
session_start();
include('../init/database.php');

$id = $_GET['id'];
$connexion->query("SET NAMES UTF8");
$query = "select un, deux, trois, quatre, cinq, six, remun, remdeux, remtrois, remunsans, remdeuxsans, remtroissans, nbBulletins from devis inner join infosdevis on infosdevis.devis=devis.id where devis.id=".$id;
$tab = array();
$req = $connexion->query($query);
while ($ligne = $req->fetch(PDO::FETCH_ASSOC)) 
{
    $un = ($ligne['un'] - $ligne['remun']) / 12;
    $deux = ($ligne['deux'] - $ligne['remdeux']) / 12;
    $trois = ($ligne['trois'] - $ligne['remtrois']) / 12;
    $quatre = ($ligne['quatre'] - $ligne['remunsans']) / 12;
    $cinq = ($ligne['cinq'] - $ligne['remdeuxsans']) / 12;
    $six = ($ligne['six'] - $ligne['remtroissans']) / 12;
    $nbBulletins=$ligne['nbBulletins'];
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Devis en ligne</title>
        <style>
            @media print  
            {
                div{
                    page-break-inside: avoid;
                }
            }
            span.un {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:80px;
                right:820px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
            span.deux {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:80px;
                right:660px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
            span.trois {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:80px;
                right:500px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
            span.quatre {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:80px;
                right:365px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
            span.cinq {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:80px;
                right:205px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
            span.six {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:80px;
                right:60px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
            span.nbBulletins {
                width:40%; 
                float:right;
                color:black; 
                background-color:transparent;
                padding:10px; 
                position:absolute; 
                top:602px;
                right:355px;
                text-align:right;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 13pt;
            }
        </style>
    </head>
    <body style='height: 100%;width:100%'>
        <!-- page content -->
        <div style="width:1000px;">
            <div class="">

                <div class="clearfix"></div>
            <div class="row">
                <center>
                    <img src="../images/mission/offreDiapo8.PNG" width="97%">
                <?php
                    echo '<span class="un">'.round($quatre,0).'€</span>';
                    echo '<span class="deux">'.round($un,0).'€</span>';
                    echo '<span class="trois">'.round($cinq,0).'€</span>';
                    echo '<span class="quatre">'.round($deux,0).'€</span>';
                    echo '<span class="cinq">'.round($six,0).'€</span>';
                    echo '<span class="six">'.round($trois,0).'€</span>';
                    //echo '<span class="nbBulletins">'.$nbBulletins.'</span>';
                    ?>
            </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
  </body>
</html>