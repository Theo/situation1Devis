<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");
$query="select saisie, nbFacturesVente, logiciel, transmissionFactures, transmissionFacturesVente, nbFacturesAchat, nbCategorieCa, transmissionFactures, transmissionNotesFrais, nbCategorieAchat, nbTauxTVA, caisse, periodicite, transmissionCaisse, quiTVA from infosdevis as i
INNER JOIN devis as d ON d.id = i.devis
where d.id=".$id;
$tab=array();
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if ($ligne['logiciel']==NULL)
    {
        $tab[0]='Non renseigné';
    }
    else
    {
        array_push($tab, $ligne['logiciel']);
    }
    array_push($tab, $ligne['transmissionFactures']);
    array_push($tab, $ligne['transmissionFacturesVente']);
    array_push($tab, $ligne['transmissionNotesFrais']);
    array_push($tab, $ligne['periodicite']);
    array_push($tab, $ligne['transmissionCaisse']);
    if ($ligne['saisie']==0)
    {
        $saisie='le client';
    }
    else 
    {
        $saisie='le cabinet';
    }
    $facturevente=$ligne['nbFacturesVente'];
    $facturachat=$ligne['nbFacturesAchat'];
    $catCA=$ligne['nbCategorieCa'];
    $catAchat=$ligne['nbCategorieAchat'];
    $tauxtva=$ligne['nbTauxTVA'];
    $caisse=$ligne['caisse'];
    $quiTVA=$ligne['quiTVA'];
} 

// Logiciel
if ($tab[0]=='Non renseigné')
{
    $logiciel='Non renseigné';
}
else
{
    $query="select nom from listes where id=".$tab[0];
    $req=$connexion->query($query);
    while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
    {
        $logiciel=$ligne['nom'];
    }
}

// Transmission facture
$query="select nom from listes where id=".$tab[1];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $transmissionfacture=$ligne['nom'];
}

// Transmission facture vente
$query="select nom from listes where id=".$tab[2];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $transmissionfacturevente=$ligne['nom'];
}

// Transmission notes de frais
$query="select nom from listes where id=".$tab[3];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $transmissionnotesfrais=$ligne['nom'];
}

// Périodicité TVA
if ($tab[4]=='160')
{
    $periodicite='Le Client ou non assujetti';
}
if ($tab[4]=='161')
{
    $periodicite='Mensuelle';
}
if ($tab[4]=='162')
{
    $periodicite='Trimestrielle';
}
if ($tab[4]=='163')
{
    $periodicite='Annuelle';
}
/*
$query="select nom from listes where id=".$tab[4];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $periodicite=$ligne['nom'];
}
*/
// Méthode transmission caisse
if ($caisse=="non")
{
    $transmissioncaisse='Pas de caisse';
}
else
{
    $query="select nom from listes where id=".$tab[5];
    $req=$connexion->query($query);
    while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
    {
        $transmissioncaisse=$ligne['nom'];
    }    
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>

    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
    span.saisie {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:92px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.facturevente {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:132px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.logiciel {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:180px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.transmissionfacture {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:225px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.factureachat {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:268px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.catca {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:314px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.transmissionfacturevente {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:365px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.transmissionnotesfrais {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:400px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.catachat {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:455px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.tauxtva {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:500px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.caisse {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:550px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.periodicite {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:590px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.transmissioncaisse {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:640px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.quiTVA{
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:680px;
        left:490px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.colorRed {
    color:red;
    font-weight: bold;
}
    </style>
  </head>
  <body style='height: 100%;width:100%'>
        <!-- page content -->
        <div style="width:1000px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <img src="../images/devis/Capture.PNG" width="95%"><br/>
                    <?php
                    echo '<span class="saisie">Qui fait la saisie comptable<br><span class="colorRed">'.$saisie.'</span></span>';
                    echo '<span class="facturevente">Estimation du nombre de factures de vente/mois<br><span class="colorRed">'.$facturevente.'</span></span>';
                    echo '<span class="logiciel">Quel logiciel de saisie<br><span class="colorRed">'.$logiciel.'</span></span>';
                    echo '<span class="transmissionfacture">Méthode de transmission des factures<br><span class="colorRed">'.$transmissionfacture.'</span></span>';
                    echo '<span class="factureachat">Estimation du nombre de factures d\'achats/mois<br><span class="colorRed">'.$facturachat.'</span></span>';
                    echo '<span class="catca">Nombre de catégories de CA<br><span class="colorRed">'.$catCA.'</span></span>';
                    echo '<span class="transmissionfacturevente">Méthode de transmission des factures<br><span class="colorRed">'.$transmissionfacturevente.'</span></span>';
                    echo '<span class="transmissionnotesfrais">Méthode de transmission des notes de frais<br><span class="colorRed">'.$transmissionnotesfrais.'</span></span>';
                    echo '<span class="catachat">Nombre de catégories d\'achats<br><span class="colorRed">'.$catAchat.'</span></span>';
                    echo '<span class="tauxtva">Nombre de taux de TVA<br><span class="colorRed">'.$tauxtva.'</span></span>';
                    echo '<span class="caisse">Avez-vous une caisse<br><span class="colorRed">'.$caisse.'</span></span>';
                    echo '<span class="periodicite">Périodicité de la TVA<br><span class="colorRed">'.$periodicite.'</span></span>';
                    echo '<span class="transmissioncaisse">Méthode de transmission de la caisse<br><span class="colorRed">'.$transmissioncaisse.'</span></span>';
                    echo '<span class="quiTVA">Qui fait la TVA<br><span class="colorRed">'.$quiTVA.'</span></span>';
                    ?>
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://1330.304.1304.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->