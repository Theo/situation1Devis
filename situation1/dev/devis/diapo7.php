<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");
$query="select bulletinspaie, transmissionVariablesPaie, nbBulletins, commissaireComptes from infosdevis as i
INNER JOIN devis as d ON d.id = i.devis
where d.id=".$id;
$tab=array();
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    array_push($tab, $ligne['bulletinspaie']);
    array_push($tab, $ligne['transmissionVariablesPaie']);
    $commissaire=$ligne['commissaireComptes'];
    $nbbulletins=$ligne['nbBulletins'];
}

// Bulletins de paie
$query="select nom from listes where id=".$tab[0];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $quiBulletin=$ligne['nom'];
}

// Transmission variables paie
$query="select nom from listes where id=".$tab[1];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $paie=$ligne['nom'];
}

if ($commissaire=="")
{
    $commissaire="oui";
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>

    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
span.quiBulletin {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:215px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.TransmissionPaie {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:272px;
        left:480px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.nbBulletin {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:320px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.commissaire {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:373px;
        left:480px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.imageBarre {
        position:absolute; 
        top:100px;
        left:447px;
        
}
span.colorPurple {
    color:purple;
    font-weight: bold;
}
    </style>
    <!-- ajouter pour droite -->
  </head>
  <body style='height: 100%;width:100%'>
        <!-- page content -->
        <div style="width:1000px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <img src="../images/mission/bandeauDiapo7.PNG" width="92%">
                    <span class="imageBarre">
                        <img src="../images/mission/barreDiapo7.PNG" width="80%">
                    </span>
                    <?php
                    echo '<span class="quiBulletin">Qui fait les bulletins de paie<br><span class="colorPurple">'.$quiBulletin.'</span></span>';
                    echo '<span class="TransmissionPaie">Transmission des variables de paie<br><span class="colorPurple">'.$paie.'</span></span>';
                    echo '<span class="nbBulletin">Nombre de bulletins de paie<br><span class="colorPurple">'.$nbbulletins.'</span></span>';
                    echo '<span class="commissaire">Avec-vous un commissaire aux comptes<br><span class="colorPurple">'.$commissaire.'</span></span>';
                    ?>
            </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
  </body>
</html>
<!-- print via linux: xvfb-run wkhtmltopdf http://1330.304.1304.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->