<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];


$connexion->query("SET NAMES UTF8");
$query="SELECT listes.nom, nbDirigeant, chiffresAffaires, nbAssocies,secteur, nbSalaries,international
FROM entreprise
INNER JOIN listes ON entreprise.statut = listes.id
INNER JOIN devis ON entreprise.id = devis.entreprise
where devis.id =".$id;

$req=$connexion->query($query);
$tab=array();
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $formeJuridique=$ligne['nom'];
    $nbDirigeant=$ligne['nbDirigeant'];
    $ca=$ligne['chiffresAffaires'];
    $nbAssocies=$ligne['nbAssocies'];
    array_push($tab, $ligne['secteur']);
    $nbSalarie=$ligne['nbSalaries'];
    $activite=$ligne['international'];
}   

// Secteur d'activité
$query="select nom from listes where id=".$tab[0];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $secteur=$ligne['nom'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>

    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
span.formeJuridique {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:420px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.nbDirigeant {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:463px;
        left:485px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.ca {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:503px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.nbAssocies {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:547px;
        left:485px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.secteur {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:588px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.nbSalarie {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:632px;
        left:485px;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.activite {
        width:60%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:669px;
        right:550px;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.colorBlue {
    color:blue;
    font-weight: bold;
}
span.imageBarre {
        position:absolute; 
        top:345px;
        left:442px;
}
    </style>
  </head>
  <body style='height: 100%;width:100%'>
        <!-- page content -->
        <div style="width:1000px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <img src="../images/mission/pageBleu.PNG" width="92%">
                    <span class="imageBarre">
                    <img src="../images/mission/barreBleuGrande.PNG" width="80%">
                    </span>
                        <?php
                    echo '<span class="formeJuridique">Forme juridique<br><span class="colorBlue">'.$formeJuridique.'</span></span>';
                    echo '<span class="nbDirigeant">Nombre de gérants/dirigeants<br><span class="colorBlue">'.$nbDirigeant.'</span></span>';
                    echo '<span class="ca">Chiffre d\'affaires<br><span class="colorBlue">'.$ca.'</span></span>';
                    echo '<span class="nbAssocies">Nombre d\'associés<br><span class="colorBlue">'.$nbAssocies.'</span></span>';
                    echo '<span class="secteur">Secteur d\'activité<br><span class="colorBlue">'.$secteur.'</span></span>';
                    echo '<span class="nbSalarie">Salariés<br><span class="colorBlue">'.$nbSalarie.'</span></span>';
                    echo '<span class="activite">Activité internationale<br><span class="colorBlue">'.$activite.'</span></span>';
                    ?>
            </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
  </body>
</html>
<!-- print via linux: xvfb-run wkhtmltopdf http://1330.304.1304.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->