<?php
session_start();
include('include/header.php');
include('init/database.php');
if(isset($_GET['id']))
{
$connexion->query("SET NAMES UTF8");
$query="select * from equipe where id=".$_GET['id'];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $nom=$ligne['nom'];
}
?>
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
        <!-- page content -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Administration <small>Gestion des objectifs équipes</small></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Modification des objectifs de l'équipe : <?php echo $nom  ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form id="ajout" data-parsley-validate class="form-horizontal form-label-left" autocomplete="off"  method="POST" Action="objectifbase.php">
                      <div class="form-group">
                                  <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Année comptable sélectionnée *</label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="date" id="date" class="form-control col-md-7 col-xs-12">
                                        <option value="-1">-- Veuillez choisir une année comptable --</option>
                                      <?php
                                            $moisAUj=date('n');
                                            $anneeAuj=date('Y');
                                            if ($moisAUj<=9)
                                            {
                                                $anneeAuj=$anneeAuj-1;
                                            }
                                            $dateGauche="01-09";
                                            for ($i=0; $i<5; $i++)
                                            {
                                                echo '<option value="'.$anneeAuj.'-09-01">Année comptable '.$anneeAuj.'';
                                                $anneeAuj++;
                                                echo ' - '.$anneeAuj.'</option>';
                                            }
                                      ?>
                                    </select>
                                  </div>
                        </div>
                      
                      <div class="form-group">
                        <label for="howmuch" class="control-label col-md-3 col-sm-3 col-xs-12">Objectif voulu</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" type="number" name="howmuch" id="howmuch"  step="1" value="0" min="0" max="100000000">
                        </div>
                      </div>
                       
                        <input type="hidden" name="equipe" value="<?php echo $_GET['id']; ?> ">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Annuler</button>
                          <button type="submit" class="btn btn-success">Valider</button>
                        </div>
                      </div>

                    </form>
                    
                    
                  </div>
                </div>
              </div>
            </div>
         </div>
        </div>
  
  <?php

include('include/footer.php');
?>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>
        <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </body>
</html>

  <?php
  header('Location: objectifequipe.php');
}
?>