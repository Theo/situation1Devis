<?php
session_start();
include('init/database.php');
include('include/header.php');

function fichierExistant($path) 
{
    $chemin=$path;
    if(file_exists($chemin))
    {
        return true;
    }
    else
    {
        return false;
    }
}
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Devis <small>Liste des documents liés au devis n°<?php echo $_GET['id']; ?></small></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste de mes documents</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Cliquez sur le document pour l'ouvrir dans une nouvelle page.
                    </p>
                    <h5><b><u>Documents:</u></b></h5>
                          <ul class="list-unstyled project_files">
                              <?php
                              $chemin='/var/www/devis/dev/mission/'.$_GET['id'].'/';
                              $file='mission/'.$_GET['id'].'/mission.pdf';
                              if(fichierExistant($chemin) == true)
                              {
                                  echo '<li><a href="'.$file.'" target=_BLANK><i class="fa fa-file-pdf-o"></i> Lettre de mission</a></li>';
                              }
                              $chemin='/var/www/devis/dev/bon-commande/'.$_GET['id'].'/';
                              $file='bon-commande/'.$_GET['id'].'/bon-de-commande.pdf';
                              if(fichierExistant($chemin) == true)
                              {
                                  echo '<li><a href="'.$file.'" target=_BLANK><i class="fa fa-file-pdf-o"></i> Bon de commande</a></li>';
                              }        
                              $chemin='/var/www/devis/dev/devis/'.$_GET['id'].'/';
                              $file='devis/'.$_GET['id'].'/devis.pdf';
                              if(fichierExistant($chemin) == true)
                              {
                                  echo '<li><a href="'.$file.'" target=_BLANK><i class="fa fa-file-pdf-o"></i> Devis</a></li>';
                              }
                              $connexion->query("SET NAMES UTF8");
                              $query="select chemin from fichier where devis=".$_GET['id']." order by id desc limit 1";
                              $req=$connexion->query($query);
                              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                              {
                                  echo '<li><a href="'.$ligne['chemin'].'" target=_BLANK><i class="fa fa-file-pdf-o"></i>Offre personnalisée</a></li>';
                              }
                              ?>
                            
                           
                          </ul>
                    <h5><b><u>Pages:</u></b></h5>
                    <a href="ventilation.php?id=<?php echo $_GET['id']?>">Ventilation</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php

include('include/footer.php');
?>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>

        <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>

    
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </body>
</html>