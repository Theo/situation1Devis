<?php
session_start();
ini_set('error_reporting', E_ALL);
include('include/header.php');
include('init/database.php');
if(isset($_GET['action']))
{
    if($_GET['action'] == "delete")
    {
        if(isset($_GET['id']))
        {
            $connexion->query("SET NAMES UTF8");
            if($delete=$connexion->exec("delete from utilisateur where id=".$_GET['id']))
            {
                $deleteUser=1;
            }
            else
            {
                $deleteUser=0;
            }
        }
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //something posted

    if (isset($_POST['cancel'])) {
        //rien
    } else {
        if(isset($_POST['action']))
        {
            $date = date('Y-m-d H:i:s');
            if($_POST['action'] == "ajout")
            {
                $connexion->query("SET NAMES UTF8");
                if($insert=$connexion->exec("insert into utilisateur(nom,prenom,mail,username,password,actif,profile,equipe,telephone) values('".$_POST['nom']."','".$_POST['prenom']."','".$_POST['mail']."','".$_POST['identifiant']."','".sha1($_POST['password'])."','".$_POST['actif']."','".$_POST['profile']."','".$_POST['equipe']."','".$_POST['telephone']."')"))
                {
                    $ajoutUser=1;
                }
                else
                {
                    $ajoutUser=0;
                }
            }
            if($_POST['action'] == "modification")
            {
                $connexion->query("SET NAMES UTF8");
                if($_POST['password']!="applidevcgenial")
                {
                    $query="update utilisateur set dateUpdated='".$date."',nom='".$_POST['nom']."',prenom='".$_POST['prenom']."',mail='".$_POST['email']."',username='".$_POST['identifiant']."',password='".sha1($_POST['password'])."',actif='".$_POST['actif']."',profile='".$_POST['profile']."',equipe='".$_POST['equipe']."',telephone='".$_POST['telephone']."' where id=".$_POST['id'];
                }
                else
                {
                    $query="update utilisateur set dateUpdated='".$date."',nom='".$_POST['nom']."',prenom='".$_POST['prenom']."',mail='".$_POST['email']."',username='".$_POST['identifiant']."',actif='".$_POST['actif']."',profile='".$_POST['profile']."',equipe='".$_POST['equipe']."',telephone='".$_POST['telephone']."' where id=".$_POST['id'];
                }
                if($modification=$connexion->exec($query))
                {
                    $modifUser=1;
                }
                else
                {
                    $modifUser=0;
                }
            }
        }
    }
}
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Administration <small>Gestion des utilisateurs</small></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste des utilisateurs</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="col-md-4">
                          <p class="text-muted font-13 m-b-30">
                            Exportez vos utilisateurs grâce aux boutons ci-dessous.
                          </p>
                      </div>
                      <div class="col-md-8">
                          <p class="text-muted"><a href="ajout-utilisateur.php"><i class="fa fa-plus-square fa-lg" aria-hidden="true"> Ajouter un utilisateur</i></a></p>
                      </div>
                    <table id="datatable-users" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                          <th>Nom</th>
                          <th>Droits</th>
                          <th>Actif</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                            $connexion->query("SET NAMES UTF8");
                            $query="select u.nom as nom,u.prenom as prenom,pu.nom as profile,u.actif as actif,u.id as id from utilisateur u INNER JOIN profileuser pu ON pu.id = u.profile";
                            $req=$connexion->query($query);
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                echo'<tr>
                                        <td>'.$ligne['nom'].' '.$ligne['prenom'].'</td>
                                        <td>'.$ligne['profile'].'</td>
                                        <td align=center>'; if($ligne['actif']=="on"){echo '<i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i>';}else{echo '<i class="fa fa-ban fa-lg" aria-hidden="true"></i>';} echo'</td>
                                        <td><span style=" cursor: pointer;" onClick="modify('.$ligne['id'].')"><i style="font-size:1.7em;" class="fa fa-pencil-square-o"></i></span>&nbsp;&nbsp;<span onClick="suppressionUser('.$ligne['id'].')" style=" cursor: pointer;"><i style="font-size:1.7em;" class="fa fa-trash-o"></i></span></td>
                                    </tr>';
                            }
                          ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <?php

include('include/footer.php');
?>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-users").length) {
            $("#datatable-users").DataTable({
              dom: "Bfrtip",
              "language": {
                "lengthMenu": "Afficher _MENU_ entrée par page",
                "zeroRecords": "Rien n'a été trouvé - désolé",
                "info": "Affichage de la page _PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données disponible",
                "infoFiltered": "(Filtrage à partir de _MAX_ lignes)",
                "search": "Recherche ",
                "paginate": {
                      "first":      "Premier",
                      "last":       "Dernier",
                      "next":       "Suivant",
                      "previous":   "Précédent"
                      },
                },
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();
        TableManageButtons.init();
      });
       $('#user tbody').on( 'click', 'span', function () {
                    var data = table.row( $(this).parents('tr') ).data();
                    if(this.id=="modifyUser")
                    {
                        modify(data[0]);
                    }
                    if(this.id=="deleteUser")
                    {
                        deleted(data[0]);
                    }
                } );
                
    function modify(id)
    {
        document.location.href="modification-utilisateur.php?id="+id;
    }
    
    function suppressionUser(id){
        bootbox.confirm('Etes-vous sûr de vouloir supprimer cet utilisateur ?', function(result) {
                if(result==true)
                {
                    var url="utilisateur.php?action=delete&id="+id;
                    window.document.location = url;
                }
                else
                {
                    // rien
                }
              }); 
    }
    </script>
    <!-- /Datatables -->
        <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    <script>
      function  deleteSuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'L\'utilisateur a bien été supprimé.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
      function deleteError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'La suppression de l\'utilisateur ne fonctionne pas. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
      function  ajoutSuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'L\'utilisateur a bien été ajouté.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
      function ajoutError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'L\'ajout de l\'utilisateur n\'a pas fonctionné. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
      function  modifySuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'L\'utilisateur a bien été modifié.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
      function modifyError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'La modification de l\'utilisateur n\'a pas fonctionné. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
    </script>
  </body>
</html>

<?php
if(isset($deleteUser))
{
    if($deleteUser == 1)
    {
        echo"<script>deleteSuccess()</script>";
    }
    else
    {
        echo"<script>deleteError()</script>";
    }
}

if(isset($ajoutUser))
{
    if($ajoutUser == 1)
    {
        echo"<script>ajoutSuccess()</script>";
    }
    else
    {
        echo"<script>ajoutError()</script>";
    }
}

if(isset($modifUser))
{
    if($modifUser == 1)
    {
        echo"<script>modifySuccess()</script>";
    }
    else
    {
        echo"<script>modifyError()</script>";
    }
}
?>