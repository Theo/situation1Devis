<?php
session_start();
include('init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");

$query="select * from devis where id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $prixpackun=($ligne['un']+$ligne['remun'])/12; 
    $prixpackun=number_format((float)$prixpackun, 2, '.', '');
    
    $prixpackdeux=($ligne['deux']+$ligne['remdeux'])/12; 
    $prixpackdeux=number_format((float)$prixpackdeux, 2, '.', '');
    
    $prixpacktrois=($ligne['trois']+$ligne['remtrois'])/12; 
    $prixpacktrois=number_format((float)$prixpacktrois, 2, '.', '');

    $prixpackunsans=($ligne['quatre']+$ligne['remunsans'])/12; 
    $prixpackunsans=number_format((float)$prixpackunsans, 2, '.', '');
    
    $prixpackdeuxsans=($ligne['cinq']+$ligne['remdeuxsans'])/12; 
    $prixpackdeuxsans=number_format((float)$prixpackdeuxsans, 2, '.', '');
    
    $prixpacktroissans=($ligne['six']+$ligne['remtroissans'])/12; 
    $prixpacktroissans=number_format((float)$prixpacktroissans, 2, '.', '');
}
$query="select * from infosdevis where devis=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $juridique=$ligne['juridique'];
    $nbbulletins=$ligne['nbBulletins'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
    </style>
  </head>
        <!-- page content -->
        <div style="width:1000px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <br/><br/>
                <div style="width:33%;float:left">
                    <div style="width:45%;float:left">
                        <span>
                            <i>Avec saisie<br/>&nbsp;&nbsp; <b><?php echo $prixpackun; ?> €</b> HT/mois</i>
                        </span>
                    </div>
                    <div style="width:45%;float:left">
                        <span>
                            <i>Sans saisie<br/> <b><?php echo $prixpackunsans; ?> €</b> HT/mois</i>
                        </span>
                    </div>
                </div>
                <div style="width:33%;float:left">
                    <div style="margin-left:30px;width:45%;float:left">
                        <span>
                            <i>Avec saisie<br/> <b><?php echo $prixpackdeux; ?> €</b> HT/mois</i>
                        </span>
                    </div>
                    <div style="width:45%;float:left">
                        <span>
                            <i>Sans saisie<br/> <b><?php echo $prixpackdeuxsans; ?> €</b> HT/mois</i>
                        </span>
                    </div>
                </div>
                <div style="width:33%;float:left">
                    <div style="margin-left:30px;width:50%;float:left">
                        <span>
                            <i>Avec saisie<br/>&nbsp;&nbsp; <b><?php echo $prixpacktrois; ?> €</b> HT/mois</i>
                        </span>
                    </div>
                    <div style="width:40%;float:left">
                        <span>
                            <i>Sans saisie<br/> <b><?php echo $prixpacktroissans; ?> €</b> HT/mois</i>
                        </span>
                    </div>
                </div>
                        <?php
                        if($juridique == "-1")
                        {
                           echo '<img src="images/offres.png" width="100%">';
                        }
                        else
                        {
                           echo '<img src="images/offresansjuridique.png" width="100%">';
                        }
                        ?>
                        <br/><br/>
                        <div style="width:95%">
                            <table style="width:100%;border: 1px solid #6fa1a1 !important;">
                                <col width="190">
                                <col width="5">
                                <col width="10">
                                <tr style="background-color:#6fa1a1 !important">
                                    <td height="50" style="color:white !important"><b style="margin-left:10px;color:white !important">Votre social</b>  : Les bulletins de payes et déclarations de charges sont inclus dans le tarif du pack. </td>
                                    <td style="background-color:white !important"></td>
                                    <td style="color:white !important;text-align:center;"><font style="margin-left:5px;">Nous avons retenu</font><br/><b style="margin-left:5px;"><?php echo $nbbulletins; ?> / bulletins / mois</b></td> 
                                </tr>
                                <tr>
                                    <td height="50"><div style="margin-left:10px;"><b>Social exceptionnel</b>: Entrée et sortie de salarié / licenciement / contrat / déclaration d’arrêt / simulation …</div></td>
                                  <td style="background-color:#6fa1a1 !important"></td>
                                  <td style="font-size: 11px;"><div style="margin-left:5px;text-align:center;">Facturation à la tache selon grille en annexe</div></td>
                                </tr>
                            </table>
                        </div>
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://137.74.174.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->