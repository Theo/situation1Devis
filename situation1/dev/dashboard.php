<?php
    session_start();
    if (isset($_GET['id']))
    {
        $equipe=$_GET['id'];
    }
    else{
        $equipe=$_SESSION['equipe'];
    }
    include('include/header.php');
    include('init/database.php');

    /*
    $moisAUj=date('n');
    $annee=date('Y');
    $compteurObj=0;
    if ($moisAUj<=9)
    {
        $annee=$annee-1;
    }
    $query="select objectifs from objectif
    where YEAR ( CONVERT(datedebut,DATE) ) between '".$annee."-09-01' and ";
    $annee++;
    $query.="'".$annee."-08-31' and equipe=".$equipe."";
    */
    $query="select objectifs from objectif
    where datedebut=".date('Y')."
    and equipe=".$equipe;
    $req=$connexion->query($query);
    while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
    {
        $compteurObj=1;
    }
    if ($compteurObj==1)
    {
        $_SESSION['objectifSet']=1;
    }
    else
    {
        $_SESSION['objectifSet']=0;    
    }
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <!-- stat -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-edit"></i> Total devis du groupe</span>
              <div class="count">
              <?php
              $connexion->query("SET NAMES UTF8");
              $query="SELECT COUNT( id ) AS somme FROM devis WHERE equipe=".$equipe." AND CONVERT(dateCreated,DATETIME) < CURRENT_TIMESTAMP( ) AND (CONVERT(dateCreated,DATETIME) > CURRENT_TIMESTAMP( ) - INTERVAL 180 DAY)";
              $req=$connexion->query($query);
              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
              {
                    echo $ligne['somme'];
              }
              $req->closeCursor(); 
              ?>
              </div>
              <span class="count_bottom"> Sur les 6 derniers mois</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total devis</span>
              <div class="count">
              <?php
              $connexion->query("SET NAMES UTF8");
              $query="SELECT count(id) as somme FROM devis WHERE equipe=".$equipe."";
              $req=$connexion->query($query);
              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
              {
                  echo $ligne['somme'];
              }
              $req->closeCursor(); 
              ?>
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i> Total devis validés</span>
              <div class="count">
              <?php
              $connexion->query("SET NAMES UTF8");
              $query="SELECT count(statut) as somme FROM devis WHERE equipe=".$equipe." and statut='Validé'";
              $req=$connexion->query($query);
              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
              {
                  echo $ligne['somme'];
              }
              $req->closeCursor(); 
              ?>
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-edit"></i> Devis créés ce mois</span>
              <div class="count">
              <?php
              $connexion->query("SET NAMES UTF8");
              $query="SELECT COUNT( id ) AS somme FROM devis WHERE equipe=".$equipe." AND CONVERT(dateCreated,DATETIME) < CURRENT_TIMESTAMP( ) AND (CONVERT(dateCreated,DATETIME) > CURRENT_TIMESTAMP( ) - INTERVAL 30 DAY)";
              $req=$connexion->query($query);
              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
              {
                  echo $ligne['somme'];
              }
              $req->closeCursor(); 
              ?>
              </div>
             
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-check"></i> Devis validés ce mois</span>
              <div class="count">
              <?php
              $connexion->query("SET NAMES UTF8");
              $query="SELECT COUNT( id ) AS somme FROM devis WHERE statut='Validé' and equipe=".$equipe." AND CONVERT(dateUpdated,DATETIME) < CURRENT_TIMESTAMP( ) AND (CONVERT(dateUpdated,DATETIME) > CURRENT_TIMESTAMP( ) - INTERVAL 30 DAY)";
              $req=$connexion->query($query);
              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
              {
                  echo $ligne['somme'];
              }
              $req->closeCursor(); 
              ?>
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-euro"></i> Chiffre d'affaires du mois</span>
              <div class="count">
              <?php
              $connexion->query("SET NAMES UTF8");
              $query="SELECT sum( prixdevischoisi ) AS somme FROM devis WHERE equipe=".$equipe." and statut='Validé' AND CONVERT(dateUpdated,DATETIME) < CURRENT_TIMESTAMP( ) AND (CONVERT(dateUpdated,DATETIME) > CURRENT_TIMESTAMP( ) - INTERVAL 30 DAY)";
              $req=$connexion->query($query);
              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
              {
                  if ($ligne['somme']==NULL || $ligne['somme']==0)
                  {
                    echo '0';
                  }
                  else
                  {
                    echo $ligne['somme'].'€';
                  }
              }
              $req->closeCursor(); 
              ?>
              </div>
            </div>
          </div>
          <!-- stat -->
          <!-- derniers devis réalisés -->
          <div class="row">
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Derniers devis réalisés</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                        <?php
                        $tab=array();
                        $query="SELECT MONTH ( CONVERT(dateCreated,DATETIME) )  as mois , DAY ( CONVERT(dateCreated,DATETIME) ) as jour, entreprise.nom as nom, entreprise.dirigeant as dirigeant FROM `devis` 
                        inner join entreprise on devis.entreprise=entreprise.id
                        and devis.equipe=".$equipe."
                        order by dateCreated desc limit 5";
                        $req=$connexion->query($query);
                                while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                                {
                                    array_push($tab, $ligne);
                                }
                                $req->closeCursor();  

                                for ($i=0; $i<count($tab); $i++)
                                {
                                    switch ($tab[$i]['mois']) {
                                        case 1:
                                            $tab[$i]['mois']='Janvier';
                                            break;
                                        case 2:
                                            $tab[$i]['mois']='Février';
                                            break;
                                        case 3:
                                            $tab[$i]['mois']='Mars';
                                            break;
                                        case 4:
                                            $tab[$i]['mois']='Avril';
                                            break;
                                        case 5:
                                            $tab[$i]['mois']='Mai';
                                            break;
                                        case 6:
                                            $tab[$i]['mois']='Juin';
                                            break;
                                        case 7:
                                            $tab[$i]['mois']='Juillet';
                                            break;
                                        case 8:
                                            $tab[$i]['mois']='Aout';
                                            break;
                                        case 9:
                                            $tab[$i]['mois']='Sept';
                                            break;
                                        case 10:
                                            $tab[$i]['mois']='Oct';
                                            break;
                                        case 11:
                                            $tab[$i]['mois']='Nov';
                                            break;
                                        case 12:
                                            $tab[$i]['mois']='Déc';
                                            break;
                                    }
                                }
                                if(count($tab)>=1){
                                    echo '<article class="media event">';
                              echo '<a class="pull-left date">';
                                 echo '<p class="month">'.$tab[0]['mois'].'</p>';
                                echo '<p class="day">'.$tab[0]['jour'].'</p>';
                              echo '</a>';
                              echo '<div class="media-body">';
                                echo '<a class="title" href="#">'.$tab[0]['nom'].'</a>';
                                echo '<p>'.$tab[0]['dirigeant'].'</p>';
                              echo '</div>';
                            echo '</article>';
                                }
                            if(count($tab)>=2){
                            echo '<article class="media event">';
                              echo '<a class="pull-left date">';
                                 echo '<p class="month">'.$tab[1]['mois'].'</p>';
                                echo '<p class="day">'.$tab[1]['jour'].'</p>';
                              echo '</a>';
                              echo '<div class="media-body">';
                                echo '<a class="title" href="#">'.$tab[1]['nom'].'</a>';
                                echo '<p>'.$tab[1]['dirigeant'].'</p>';
                              echo '</div>';
                            echo '</article>';
                            }
                            if(count($tab)>=3){
                              echo '<article class="media event">';
                              echo '<a class="pull-left date">';
                                 echo '<p class="month">'.$tab[2]['mois'].'</p>';
                                echo '<p class="day">'.$tab[2]['jour'].'</p>';
                              echo '</a>';
                              echo '<div class="media-body">';
                                echo '<a class="title" href="#">'.$tab[2]['nom'].'</a>';
                                echo '<p>'.$tab[2]['dirigeant'].'</p>';
                              echo '</div>';
                            echo '</article>';
                            }
                            if(count($tab)>=4){
                              echo '<article class="media event">';
                              echo '<a class="pull-left date">';
                                 echo '<p class="month">'.$tab[3]['mois'].'</p>';
                                echo '<p class="day">'.$tab[3]['jour'].'</p>';
                              echo '</a>';
                              echo '<div class="media-body">';
                                echo '<a class="title" href="#">'.$tab[3]['nom'].'</a>';
                                echo '<p>'.$tab[3]['dirigeant'].'</p>';
                              echo '</div>';
                            echo '</article>';
                            }
                            if(count($tab)>=5){
                              echo '<article class="media event">';
                              echo '<a class="pull-left date">';
                                 echo '<p class="month">'.$tab[4]['mois'].'</p>';
                                echo '<p class="day">'.$tab[4]['jour'].'</p>';
                              echo '</a>';
                              echo '<div class="media-body">';
                                echo '<a class="title" href="#">'.$tab[4]['nom'].'</a>';
                                echo '<p>'.$tab[4]['dirigeant'].'</p>';
                              echo'  </div>';
                            echo'  </article>';}?>
                </div>
              </div>
              </div>
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Derniers devis validés</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                          <?php
                        $tab=array();
                        $query="SELECT MONTH ( CONVERT(dateUpdated,DATETIME) )  as mois , DAY ( CONVERT(dateUpdated,DATETIME) ) as jour, entreprise.nom as nom, entreprise.dirigeant as dirigeant FROM `devis` 
                        inner join entreprise on devis.entreprise=entreprise.id
                        where devis.equipe=".$equipe."
                        and devis.statut='Validé'
                        order by dateUpdated desc limit 5";
                        $req=$connexion->query($query);
                        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                        {
                            array_push($tab, $ligne);
                        }
                        $req->closeCursor();  
                        for ($i=0; $i<count($tab); $i++)
                        {
                            switch ($tab[$i]['mois']) {
                                case 1:
                                    $tab[$i]['mois']='Janvier';
                                    break;
                                case 2:
                                    $tab[$i]['mois']='Février';
                                    break;
                                case 3:
                                    $tab[$i]['mois']='Mars';
                                    break;
                                case 4:
                                    $tab[$i]['mois']='Avril';
                                    break;
                                case 5:
                                    $tab[$i]['mois']='Mai';
                                    break;
                                case 6:
                                    $tab[$i]['mois']='Juin';
                                    break;
                                case 7:
                                    $tab[$i]['mois']='Juillet';
                                    break;
                                case 8:
                                    $tab[$i]['mois']='Aout';
                                    break;
                                case 9:
                                    $tab[$i]['mois']='Sept';
                                    break;
                                case 10:
                                    $tab[$i]['mois']='Oct';
                                    break;
                                case 11:
                                    $tab[$i]['mois']='Nov';
                                    break;
                                case 12:
                                    $tab[$i]['mois']='Déc';
                                    break;
                            }
                        }
                        
                        if(count($tab)>=1){
                            echo'<article class="media event">';
                      echo '<a class="pull-left date">';
                       echo '<p class="month">'.$tab[0]['mois'].'</p>';
                        echo '<p class="day">'.$tab[0]['jour'].'</p>';
                      echo '</a>';
                      echo '<div class="media-body">';
                        echo '<a class="title" href="#">'.$tab[0]['nom'].'</a>';
                        echo '<p>'.$tab[0]['dirigeant'].'</p>';
                      echo '</div>';
                    echo '</article>';
                        }
                      if(count($tab)>=2){  
                    echo '<article class="media event">';
                      echo '<a class="pull-left date">';
                         echo '<p class="month">'.$tab[1]['mois'].'</p>';
                        echo '<p class="day">'.$tab[1]['jour'].'</p>';
                      echo '</a>';
                      echo '<div class="media-body">';
                        echo '<a class="title" href="#">'.$tab[1]['nom'].'</a>';
                        echo '<p>'.$tab[1]['dirigeant'].'</p>';
                      echo '</div>';
                    echo '</article>';
                      }
                      if(count($tab)>=3){
                      echo '<article class="media event">';
                      echo '<a class="pull-left date">';
                         echo '<p class="month">'.$tab[2]['mois'].'</p>';
                        echo '<p class="day">'.$tab[2]['jour'].'</p>';
                      echo '</a>';
                      echo '<div class="media-body">';
                        echo '<a class="title" href="#">'.$tab[2]['nom'].'</a>';
                        echo '<p>'.$tab[2]['dirigeant'].'</p>';
                      echo '</div>';
                    echo '</article>';
                      }
                      if(count($tab)>=4){
                      echo '<article class="media event">';
                      echo '<a class="pull-left date">';
                         echo '<p class="month">'.$tab[3]['mois'].'</p>';
                        echo '<p class="day">'.$tab[3]['jour'].'</p>';
                      echo '</a>';
                      echo '<div class="media-body">';
                        echo '<a class="title" href="#">'.$tab[3]['nom'].'</a>';
                        echo '<p>'.$tab[3]['dirigeant'].'</p>';
                      echo '</div>';
                    echo '</article>';
                      }
                      if(count($tab)>=5){
                      echo '<article class="media event">';
                      echo '<a class="pull-left date">';
                         echo '<p class="month">'.$tab[4]['mois'].'</p>';
                        echo '<p class="day">'.$tab[4]['jour'].'</p>';
                      echo '</a>';
                      echo '<div class="media-body">';
                        echo '<a class="title" href="#">'.$tab[4]['nom'].'</a>';
                        echo '<p>'.$tab[4]['dirigeant'].'</p>';
                      echo '</div>';
                    echo '</article>';}?>
                </div>
              </div>
            </div>
          </div>
          <!-- derniers devis réalisés -->
          
          <div class="row">
            <div class="col-md-4">
                <div class="x_panel tile fixed_height_320">
                  <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Mon objectif<small>Année <?php echo date('Y'); ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                      </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="dashboard-widget-content">
                      <ul class="quick-list">

                        <?php
                        // TEST SI OBJECTIF 
                        // SI OUI AFFICHER SINON AFFICHER 'AUCUN OBJECTIF'
                        if ($_SESSION['objectifSet']==1)
                        {
                            /*
                            $moisAUj=date('n');
                            $annee=date('Y');
                            if ($moisAUj<=9)
                            {
                                $annee=$annee-1;
                            }
                            
                            $query="select objectifs from objectif
                            where YEAR ( CONVERT(datedebut,DATE) ) between '".$annee."-09-01' and ";
                            $annee++;
                            $query.="'".$annee."-08-31' and equipe=".$equipe."";
                            */
                            $query="SELECT objectifs
                            FROM objectif
                            WHERE datedebut =  ".date('Y')."
                            AND equipe =".$equipe."
                            ORDER BY id DESC 
                            LIMIT 1";
                            $req=$connexion->query($query);
                            $compteur=0;
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                $compteur=1;
                                echo '<li><i class="fa fa-euro"></i>But : '.$ligne['objectifs'].' €</li>';
                                $_SESSION['obj']=$ligne['objectifs'];
                            }

                            if ($compteur==0)
                            {
                                echo '<li><i class="fa fa-euro"></i>But : 0 €</li>'; 
                                $_SESSION['obj']=0;
                            }

                            $moisAUj=date('n');
                            $annee=date('Y');
                            if ($moisAUj<=9)
                            {
                                $annee=$annee-1;
                            }
                            $query="SELECT sum( prixdevischoisi ) AS somme FROM devis where statut='Validé'
                            and YEAR ( CONVERT(dateUpdated,DATE) ) between '".$annee."-09-01' and ";
                            $annee++;
                            $query.="'".$annee."-08-31' and equipe=".$equipe."";
                            $req=$connexion->query($query);
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                if ($ligne['somme']==NULL || $ligne['somme']==0)
                                {
                                    echo '<li><i class="fa fa-euro"></i>CA : 0 €</li>';  
                                    $_SESSION['CA']=0;
                                }
                                else 
                                {
                                   echo '<li><i class="fa fa-euro"></i>CA : '.$ligne['somme'].' €</li>';  
                                   $_SESSION['CA']=$ligne['somme'];
                                }
                            }


                            $moisAUj=date('n');
                            $annee=date('Y');
                            if ($moisAUj<=9)
                            {
                                $annee=$annee-1;
                            }
                            $query="SELECT count(statut) as somme FROM devis where statut='Validé'
                            and YEAR ( CONVERT(dateUpdated,DATE) ) between '".$annee."-09-01' and ";
                            $annee++;
                            $query.="'".$annee."-08-31' and equipe=".$equipe."";
                            $req=$connexion->query($query);
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                echo '<li><i class="fa fa-check"></i>Devis validés: '.$ligne['somme'].'</li>';
                            }

                            $moisAUj=date('n');
                            $annee=date('Y');
                            if ($moisAUj<=9)
                            {
                                $annee=$annee-1;
                            }
                            $query=" SELECT COUNT( id ) AS somme FROM devis
                            where YEAR ( CONVERT(dateCreated,DATE) ) between '".$annee."-09-01' and ";
                            $annee++;
                            $query.="'".$annee."-08-31' and equipe=".$equipe."";
                            $req=$connexion->query($query);
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                echo '<li><i class="fa fa-edit"></i>Devis réalisés:'.$ligne['somme'].'</li>';
                            }
                            $req->closeCursor();
                        }
                        else
                        {
                            echo '<li><i class="fa fa-edit"></i>Aucun objectif</li>'; 
                        }
                        ?>
                      </ul>
                      
                      <?php
                      // TEST SI IL Y A DES OBJECTIFS
                      // si oui afficher sinon ne pas afficher
                      if ($_SESSION['objectifSet']==1)
                      {
                      echo '
                      <div class="sidebar-widget">
                        <h4>Objectifs</h4>
                        <canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
                        <div class="goal-wrapper">
                            <span id="gauge-text" class="gauge-value pull-left">'.$_SESSION['CA'].'</span><span class=\'pull-left\'>&nbsp;€</span><span id=\'gauge-percentage\'></span>
                          <span id="goal-text" class="goal-value pull-right">';echo $_SESSION['obj'].' €';echo '</span>
                        </div>
                      </div> ';
                      }
                          ?>
                    </div>
                  </div>
                </div>
              </div>
            <div class="col-md-8">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2><i class="fa fa-bar-chart"></i> Mes statistiques<small>Année <?php echo date('Y'); ?></small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <canvas id="mybarChart"></canvas>
                    </div>
                  </div>
                </div>
          </div>
          </div>
        </div>
        <!-- /page content -->
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->   
    <script>
        <?php
        if ($_SESSION['objectifSet']==1)
        {
            echo 'var opts = {
                 lines: 12,
                 angle: 0,
                 lineWidth: 0.4,
                 pointer: {
                     length: 0.75,
                     strokeWidth: 0.042,
                     color: \'#1D212A\'
                 },
                 limitMax: \'false\',
                 percentColors: [[0.0, "#ff0000" ], [0.50, "#f9c802"], [1.0, "#a9d70b"]], // !!!!
                 colorStart: \'#1ABC9C\',
                 colorStop: \'#1ABC9C\',
                 strokeColor: \'#F0F3F3\',
                 generateGradient: true
             }; 
             var target = document.getElementById(\'foo\'),
             gauge = new Gauge(target).setOptions(opts);

             var ca = '; echo $_SESSION['CA']; echo ';'; echo '
             gauge.maxValue = '; echo $_SESSION['obj']; echo ';'; echo '
             gauge.animationSpeed = 50;
             if(ca > gauge.maxValue)
             {
                 gauge.set(gauge.maxValue);
             }
             else
             {
                 gauge.set(ca);
             }
             var percent=Math.floor((ca / gauge.maxValue) * 100); ';

             if ($_SESSION['CA']==0 && $_SESSION['obj']==0)
             {
                 echo 'document.getElementById(\'gauge-percentage\').textContent=0+"%"';
             }
             else
             {
             echo 'document.getElementById(\'gauge-percentage\').textContent=percent+"%"';
             }
        }
      ?>
      // Bar chart*/
      var ctx = document.getElementById("mybarChart");
      var mybarChart = new Chart(ctx, {
          scaleOverride : true,
        scaleSteps : 10,
        scaleStepWidth : 50,
        scaleStartValue : 0 ,
        type: 'bar',
        data: {
          labels: [    <?php
    $toutLesMois = array(1 => "Septembre", 2 => "Octobre", 3 => "Novembre", 4 => "Décembre", 5 => "Janvier", 6 => "Février", 7 => "Mars", 8 => "Avril", 9 => "Mai", 10 => "Juin", 11 => "Juillet", 12 => "Aout");

    for ($i=1; $i<13; $i++)
    {
        if ($i==12)
        {
            echo '"'.$toutLesMois[$i].'"';
        }
        else
        {
            echo '"'.$toutLesMois[$i].'",';
        }
    }
    // PARTIE NOM FONCTIONNE?>
          ],
        datasets: [{
            label: '# devis créés',
            backgroundColor: "#26B99A",
            data: [<?php
            $moisAUj=date('n');
        $annee=date('Y');
        if ($moisAUj<=9)
        {
            $annee=$annee-1;
        }
            $connexion->query("SET NAMES UTF8");
        $query="SELECT count( id ) AS somme, MONTH ( CONVERT(dateCreated,DATE) ) as mois FROM devis where equipe=".$equipe." and YEAR ( CONVERT(dateCreated,DATE) ) between '".$annee."-09-01' and ";
        $annee++;
        $query.="'".$annee."-08-31' group by mois";
        $req=$connexion->query($query);
        $tab=array();
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            $tab[$ligne['mois']]=$ligne['somme'];
        }
        $req->closeCursor();
        
        for ($i=1; $i<13; $i++)
        {
            if (array_key_exists($i, $tab))
            {
                // ne rien faire
            }
            else
            {
                $tab[$i]=0;
            }
        }

        ksort($tab);
        
        for ($i=9; $i<13; $i++)
        {
                echo $tab[$i].',';
        }
        
        for ($i=1; $i<9; $i++)
        {
            if ($i==8)
            {
                echo $tab[$i];
            }
            else 
            {
                echo $tab[$i].',';
            }
        }
            ?>]
          }, {
            label: '# devis validés',
            backgroundColor: "#03586A",
            data: [<?php
        $moisAUj=date('n');
        $annee=date('Y');
        if ($moisAUj<=9)
        {
            $annee=$annee-1;
        }
        $connexion->query("SET NAMES UTF8");
        $query="SELECT count( id ) AS somme, MONTH ( CONVERT(dateUpdated,DATETIME) ) as mois FROM devis where equipe=".$equipe." and statut='Validé'  and YEAR ( CONVERT(dateUpdated,DATE) ) between '".$annee."-09-01' and ";
        $annee++;
        $query.="'".$annee."-08-31' group by mois";
        $req=$connexion->query($query);
        $tab=array();
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            $tab[$ligne['mois']]=$ligne['somme'];
        }
        $req->closeCursor();

        for ($i=1; $i<13; $i++)
        {
            if (array_key_exists($i, $tab))
            {
                // ne rien faire
            }
            else
            {
                $tab[$i]=0;
            }
        }
        ksort($tab);
        
        for ($i=9; $i<13; $i++)
        {
                echo $tab[$i].',';
        }
        
        for ($i=1; $i<9; $i++)
        {
            if ($i==8)
            {
                echo $tab[$i];
            }
            else 
            {
                echo $tab[$i].',';
            }
        }
            ?>]
          }]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                max:<?php 
        $moisAUj=date('n');
        $annee=date('Y');
        if ($moisAUj<=9)
        {
            $annee=$annee-1;
        }
        $connexion->query("SET NAMES UTF8");
        $query="SELECT count( id ) AS somme, MONTH ( CONVERT(dateCreated,DATE) ) as mois FROM devis where YEAR ( CONVERT(dateCreated,DATE) ) between '".$annee."-09-01' and ";
        $annee++;
        $query.="'".$annee."-08-31' and equipe=".$equipe." group by mois";
        $req=$connexion->query($query);
        $tab=array();
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            array_push($tab, $ligne['somme']);
        }
        $req->closeCursor();
        $max=0;
        for ($i=0; $i<count($tab);$i++)
        {
            if ($max<$tab[$i])
            {
                $max=$tab[$i];
            }
        }
        
        if ($max%2==0)
        {
            $max=$max+2;
        }
        else if ($max%2==1)
        {
            $max=$max+1;
        }
        echo $max;
        ?>
              }
            }]
          }
        }
      });

    </script>
    <!-- /gauge.js -->
<?php

include('include/footer.php');