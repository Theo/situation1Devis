<?php
session_start();
include('init/database.php');
include('include/header.php');

function fichierExistant($path) 
{
    $chemin=$path;
    if(file_exists($chemin))
    {
        return true;
    }
    else
    {
        return false;
    }
}
?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Devis <small>Liste des documents liés au devis n°<?php echo $_GET['id']; ?></small></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste des coûts de ventilation</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <h5><b><u>Ventilation : </u></b></h5>
                          <ul class="list-unstyled project_files">
                              <?php        
                              $connexion->query("SET NAMES UTF8");
                            $query="select heuresjuridique, heuressocial, heuresgestion, heuresrevisionEC, heuresrevision, heuresbudget from ventilation where idDevis=".$_GET['id']."";
                              $req=$connexion->query($query);
                              while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                              {
                                  $total=round($ligne['heuresjuridique'],1)*85; echo '<li>Heures juridique : '.$total.'€ ('.round($ligne['heuresjuridique'],1).' heures à 85€/h)</li>';
                                  $total=round($ligne['heuressocial'],1)*65; echo '<li>Heures social : '.$total.'€ ('.round($ligne['heuressocial'],1).' heures à 65€/h)</li>';
                                  $total=round($ligne['heuresgestion'],1)*85; echo '<li>Heures gestion : '.$total.'€ ('.round($ligne['heuresgestion'],1).' heures à 85€/h)</li>';
                                  $total=round($ligne['heuresrevisionEC'],1)*132; echo '<li>Heures relationnel : '.$total.'€ ('.round($ligne['heuresrevisionEC'],1).' heures à 132€/h)</li>';
                                  $total=round($ligne['heuresrevision'],1)*85; echo '<li>Heures révision : '.$total.'€ ('.round($ligne['heuresrevision'],1).' heures à 85€/h)</li>';
                                  $total=round($ligne['heuresbudget'],1)*62; echo '<li>Heures budget : '.$total.'€ ('.round($ligne['heuresbudget'],1).' heures à 62€/h)</li>';
                              }
                              ?>
                          </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php

include('include/footer.php');
?>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>

        <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>

    
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </body>
</html>
