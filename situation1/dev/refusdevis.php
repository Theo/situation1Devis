<?php
session_start();
include('init/database.php');
include('include/header.php');
$connexion->query("SET NAMES UTF8");
$query="select e.nom as nom from devis d inner join entreprise e on e.id=d.entreprise where d.id=".$_GET['id'];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
   $nomSociete=$ligne['nom'];
}
?>
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
   <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
              <div id="loader">
                  <div id="divLoading">
                      
                  </div>
              </div>
            <div class="clearfix"></div>

            <div class="row">
                <h2>Description du motif de refus pour le devis n°<?php echo $_GET['id']; ?>: <strong><?php echo $nomSociete ?></strong></h2>
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12">
                          <div class="col-md-4"></div>
                          <div class="col-md-4 col-sm-6 col-xs-12" style="text-align:center">
                              <h3>Refus du devis n°<?php echo $_GET['id']; ?></h3>
                              <center>
                                  <form id="refus" action="updatestatutdevis.php" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Motif du refus: <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <textarea rows="5" cols="100"  id="motif" name="motif" required="required" class="form-control col-md-7 col-xs-12"></textarea>
                                    </div>
                                      <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>">
                                      <input type="hidden" name="statut" id="statut" value="Refusé">
                                  </div>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                      <button type="submit" class="btn btn-primary">Annuler</button>
                                      <button type="submit" class="btn btn-success">Valider</button>
                                    </div>
                                  </div>

                                </form>
                              </center>
                          </div>
                          <div class="col-md-4"></div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Devis - créer par <a href="https://applidev.fr">AppliDev'</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- Bootbox -->
    <script src="../vendors/bootbox/bootbox.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  
  </body>
</html>