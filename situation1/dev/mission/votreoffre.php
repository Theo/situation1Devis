<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");
$query="SELECT * from infosdevis where devis=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $paie=$ligne['bulletinspaie'];
    $nbbulletins=$ligne['nbBulletins'];
}
$query="SELECT * from devis where id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $budget=round($ligne['prixdevischoisi']/12,2);
    $saisie=$ligne['saisie'];
    $pack=$ligne['packchoisi'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
    span.saisie {
        width:300px; 
        float:right;
        color:#919191; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:15.3%;
        left:64.7%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15pt;
}
span.bulletins {
        width:300px;
        color:white; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:72%;
        left:65%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 10pt;
}
span.budget {
        width:300px;
        color:#727272; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:62%;
        left:61%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15pt;
}
span.options {
        width:600px;
        color:#727272; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:30%;
        left:44%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12pt;
}
    </style>
  </head>
  <body style='height: 100%;width:100%'>
        <!-- page content -->
        <div style="width:1000px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <?php
                    if($pack=="1")
                    {
                        echo '<img src="../images/mission/4/diapo1.png" width="100%">';
                    }
                    if($pack=="2")
                    {
                        echo '<img src="../images/mission/4/diapo2.png" width="100%">';
                    }
                    if($pack=="3")
                    {
                        echo '<img src="../images/mission/4/diapo333.png" width="100%">';
                    }
                    ?>
                    <span class="saisie"><?php if($saisie=="1"){echo "Le Cabinet";}else{echo "Le Client";} ?></span>
                    <span class="bulletins"><?php 
                    if($paie==165)
                        {
                        echo "Nous avons retenu<br/><b>".$nbbulletins." / bulletins /mois</b>";
                     } 
                     
                     else 
                     {
                         echo "Réalisé par vos soins";
                     }
                ?></span>
                    <span class="budget"><b><?php echo $budget; ?> €</b></span>
                    <span class="options">
                        <?php 
                            $query="SELECT l.nom as nom,(select count(*) from optionsdevis where devis=o.devis and options!=0) as count FROM `optionsdevis` o inner join listes l on l.id=o.options where o.devis=".$id." and l.option=0";
                            $req=$connexion->query($query);
                            $i=0;
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                $i++;
                                echo "- ".$ligne['nom']."<br/>";
                            }
                            if($i=="0")
                            {
                                echo "Aucune option retenue";
                            }
                        ?> 
                    </span>
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://137.74.174.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->