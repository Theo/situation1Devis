<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];
$connexion->query("SET NAMES UTF8");
$query="SELECT packchoisi FROM `devis` where id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if($ligne['packchoisi'] =="1")
    {
        $query="SELECT jun FROM `echeancier`where devis=".$id;
        $req=$connexion->query($query);
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            $juridique=$ligne['jun'];
        }
    }
    else if($ligne['packchoisi'] =="2")
    {
        $query="SELECT jdeux FROM `echeancier`where devis=".$id;
        $req=$connexion->query($query);
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            $juridique=$ligne['jdeux'];
        }
    }
    else if($ligne['packchoisi'] =="3")
    {
        $query="SELECT jtrois FROM `echeancier`where devis=".$id;
        $req=$connexion->query($query);
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            $juridique=$ligne['jtrois'];
        }
    }
}

$trimestriel=$juridique/4;
$mensuel=$juridique/12;
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
        span{
            color: #595959 !important;
        }
    }
    
    span.trimestriel {
        width:100%;
        background-color:transparent; 
        float:left; 
        padding:10px; 
        position:absolute; 
        top:24%; 
        left:24%;
        text-align:left;
        font-size: 12pt
    }
    span.mensuel {
        width:100%;
        background-color:transparent; 
        float:left; 
        padding:10px; 
        position:absolute; 
        top:28%; 
        left:28%;
        text-align:left;
        font-size: 12pt
    }
    </style>
  </head>
        <!-- page content -->
        <div style="width:1000px;margin-left:10px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                <span class="trimestriel"><b><?php echo "4 x ".round($trimestriel,2)." € HT"; ?></b></span>
                    <span class="mensuel"><b><?php  echo "12 x ".round($mensuel,2)." x 1.2 € TTC"; ?></b></span>
                    <img src="../images/mission/cgv/echeancierFulturis.png" width="100%">
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://137.74.174.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->