<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");
$query="SELECT (select nom from listes where id=i.transmissionCaisse) as methodecaisse,
(select nom from listes where id=i.transmissionFactures) as methodeachats,
(select nom from listes where id=i.transmissionFacturesVente) as methodeventes,
(select nom from listes where id=i.transmissionNotesFrais) as methodenotesfrais,
(select nom from listes where id=166) as methodepaie,
`nbFacturesAchat` as volumeachat,
`nbFacturesVente` as volumevente,
`nbBulletins` as volumepaie,
i.bulletinspaie as bulletinspaie
FROM `infosdevis` i
WHERE i.devis=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $methodecaisse=$ligne['methodecaisse'];
    $methodeachats=$ligne['methodeachats'];
    $methodeventes=$ligne['methodeventes'];
    $methodenotesfrais=$ligne['methodenotesfrais'];
    $methodepaie=$ligne['methodepaie'];
    $volumeachat=$ligne['volumeachat'];
    $volumevente=$ligne['volumevente'];
    $volumepaie=$ligne['volumepaie'];
    $quibulletinspaie=$ligne['bulletinspaie'];
}


$query="SELECT * from devis where id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $saisie=$ligne['saisie'];
    $packchoisi=$ligne['packchoisi'];
}

//echo 'saisie : '.$saisie.'<br>';
//echo 'packchoisi : '.$packchoisi.'<br>';
$query="SELECT honoraires1, honoraires2, honoraires3, honoraires4, honoraires5, honoraires6 from infosdevis where devis=".$id;
//echo 'query : '.$query;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if ($packchoisi=='1')
    {
        if ($saisie=='0')
        {
            $honoraires=$ligne['honoraires4'];
        }
        else
        {
            $honoraires=$ligne['honoraires1'];
        }
    }
    elseif ($packchoisi=='2')
    {
        if ($saisie=='0')
        {
            $honoraires=$ligne['honoraires5'];
        }
        else
        {
            $honoraires=$ligne['honoraires2'];
        }
    }
    else 
    {
        if ($saisie=='0')
        {
            $honoraires=$ligne['honoraires6'];
        }
        else
        {
            $honoraires=$ligne['honoraires3'];
        }
    }
}
//echo 'honoraires : '.$honoraires;

function icon($saisie)
{
    if($saisie=="1")
    {
        return '<i class="fa fa-check fa-lg" aria-hidden="true"></i>';
    }
    else
    {
        return '<i class="fa fa-times fa-lg" aria-hidden="true"></i>';
    }
}
function columnpercent($saisie)
{
    if($saisie=="1")
    {
        return '32%';
    }
    else
    {
        return '39.6%';
    }
}
function columnsaisiepaie($quibulletinspaie)
{
    if($quibulletinspaie=="165")
    {
        return '32%';
    }
    else
    {
        return '39.6%';
    }
}

function getsaisie($saisie)
{
    return $saisie;
}

if($packchoisi=="1")
{
    $packchamp=" prixUn ";
}
if($packchoisi=="2")
{
    $packchamp=" prixDeux ";
}
if($packchoisi=="3")
{
    $packchamp=" prixTrois ";
}



?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
        span.titre {
        width:500px;
        color:#575757; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:5%;
        left:2%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 18pt;
        }
    }
span.titre {
        width:500px;
        color:#575757; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:5%;
        left:2%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 18pt;
}
span.methodecaisse {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:17.4%;
        left:76%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.methodeachats {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:19.6%;
        left:76%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.methodeventes {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:21.8%;
        left:76%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.methodenotesfrais {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:24%;
        left:76%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.methodepaie {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:49.5%;
        left:76%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}

span.volumeachat {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:20.1%;
        left:55%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.volumevente {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:22.3%;
        left:55%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.volumepaie {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:49%;
        left:55%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}

span.revisioncomptes {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:28.8%;
        left:32%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.liassefiscale {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:31%;
        left:32%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.plaquettebilan {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:33.3%;
        left:32%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.revenustns {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:35.5%;
        left:32%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}

span.tenuecaisse {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:17.6%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.tenueachats {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:20.1%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.tenuesventes {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:22.3%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.tenuesfrais {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:24.5%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.tenuesbanque {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:26.8%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.etabtva {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:40%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.decladiverses {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:42.5%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.cice {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:44.6%;
        left:<?php echo columnpercent($saisie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.quibulletinspaie {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:48.5%;
        left:<?php echo columnsaisiepaie($quibulletinspaie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.declasociaux {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:51.2%;
        left:<?php echo columnsaisiepaie($quibulletinspaie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.decladads {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:53.2%;
        left:<?php echo columnsaisiepaie($quibulletinspaie);?>;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.bilanimage {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:58.4%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.entretienconseil {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:61.5%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.entretienconseiltemps {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:61.5%;
        left:57%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.declarationrevenus {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:64.9%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.projection {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:67.2%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.indicateursgestion {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:69.4%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.tableaubord {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:71.8%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.bilansocial {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:74%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}
span.assistancesocial {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:75.7%;
        left:37%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 6pt;
}

span.heuresJuridique {
        width:200px;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:88.8%;
        left:15%;
        text-align:center;
        font-family: Arial, Helvetica, sans-serif;
}
    </style>
  </head>

        <!-- page content -->
        <div style="width:1000px;margin-left:10px;">
          <div class="">

            <div class="clearfix"></div>
            <div class="row">
                <center>
                    <img src="../images/mission/6/tableau22.PNG" width="100%">
                    <span class="titre"><?php if(getsaisie($saisie)=="1"){echo "Saisie par le cabinet";} else{echo "Saisie par vos soins";}?></span>
                    <span class="methodecaisse"><?php if(getsaisie($saisie)=="1"){if($methodecaisse==null){echo 'Pas de caisse';}else{echo substr($methodecaisse, 0, 40);} if(strlen($methodecaisse)>40){echo"..";} } ?></span>
                    <span class="methodeachats"><?php if(getsaisie($saisie)=="1"){ echo substr($methodeachats, 0, 40); if(strlen($methodeachats)>40){echo"..";} } ?></span>
                    <span class="methodeventes"><?php if(getsaisie($saisie)=="1"){ echo substr($methodeventes, 0, 40); if(strlen($methodeventes)>40){echo"..";} } ?></span>
                    <span class="methodenotesfrais"><?php if(getsaisie($saisie)=="1"){ echo substr($methodenotesfrais, 0, 40); if(strlen($methodenotesfrais)>40){echo"..";} } ?></span>
                    <span class="methodepaie"><?php  if(getsaisie($saisie)=="1"){echo substr($methodepaie, 0, 40); if(strlen($methodepaie)>40){echo"..";} } ?></span>
                    
                    <span class="volumeachat"><?php echo substr($volumeachat, 0, 40); if(strlen($volumeachat)>40){echo"..";}  ?></span>
                    <span class="volumevente"><?php echo substr($volumevente, 0, 40); if(strlen($volumevente)>40){echo"..";}  ?></span>
                    <span class="volumepaie"><?php echo substr($volumepaie, 0, 40); if(strlen($volumepaie)>40){echo"..";}  ?></span>
                    
                    <span class="tenuecaisse"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="tenueachats"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="tenuesventes"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="tenuesfrais"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="tenuesbanque"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    
                    <span class="revisioncomptes"><?php echo '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'; ?></span>
                    <span class="liassefiscale"><?php echo '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'; ?></span>
                    <span class="plaquettebilan"><?php echo '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'; ?></span>
                    <span class="revenustns"><?php echo '<i class="fa fa-check fa-lg" aria-hidden="true"></i>'; ?></span>
                    
                    
                    <span class="etabtva"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="decladiverses"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="cice"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    
                    
                    <span class="quibulletinspaie"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="declasociaux"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    <span class="decladads"><i class="fa fa-check fa-lg" aria-hidden="true"></i></span>
                    
                    <span class="bilanimage" id="bilanimage" name="bilanimage"></span>
                    <span class="entretienconseil" id="entretienconseil" name="entretienconseil"></span>
                    <span class="entretienconseiltemps" id="entretienconseiltemps" name="entretienconseiltemps"></span>
                    <span class="declarationrevenus" id="declarationrevenus" name="declarationrevenus"></span>
                    <span class="projection" id="projection" name="projection"></span>
                    <span class="indicateursgestion" id="indicateursgestion" name="indicateursgestion"></span>
                    <span class="tableaubord" id="tableaubord" name="tableaubord"></span>
                    <span class="bilansocial" id="bilansocial" name="bilansocial"></span>
                    <span class="assistancesocial" id="assistancesocial" name="assistancesocial"></span>
                    <span class="heuresJuridique" id="heuresJuridique" name="heuresJuridique"><?php $total=round($honoraires, 1); echo $total.' €'; ?></span>
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>

<?php
$optionsdisplay=array("bilanimage","entretienconseil","entretienconseil","declarationrevenus","projection","indicateursgestion","tableaubord","bilansocial","assistancesocial");
$optionscheck=array("100","106","107","104","102","103","115","105","111");
$optionsdispo=array();
$optionsretenue=array();
$query="SELECT options FROM  `optionsdevis` o WHERE o.devis=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    array_push($optionsretenue,$ligne['options']);
}

$query="SELECT ".$packchamp." as prix, l.id as options
FROM  `pack` p
INNER JOIN listes l ON l.id = p.`idOption` 
WHERE ".$packchamp." IS NOT NULL 
AND ".$packchamp." !=  'null'
AND ".$packchamp." !=  'devis'";
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if($ligne['prix']=="0")
    {
        array_push($optionsretenue,$ligne['options']);
    }
    array_push($optionsdispo,$ligne['options']);
}

$lengthcheck = count($optionscheck);
$entretienconseilok=0;
$entretientemps="";
for ($x = 0; $x < $lengthcheck; $x++) 
{
    if(in_array($optionscheck[$x], $optionsdispo))
    {
        if(in_array($optionscheck[$x], $optionsretenue))
        {
            if($optionscheck[$x]=="107" && $entretienconseilok==1)
            {
                echo '<script>document.getElementById("entretienconseiltemps").textContent="Semestriel";</script>';
            }
            else
            {
                echo '<script>document.getElementById("'.$optionsdisplay[$x].'").textContent="Option retenue";</script>';
            }
            if($optionscheck[$x]=="106")
            {
                $entretienconseilok=1;
                echo '<script>document.getElementById("entretienconseiltemps").textContent="Semestriel";</script>';
            }
            if($optionscheck[$x]=="107" && $entretienconseilok==0)
            {
                echo '<script>document.getElementById("entretienconseiltemps").textContent="Trimestriel";</script>';
            }
        }
        else 
        {
            if($optionscheck[$x]=="107" && $entretienconseilok==1)
            {
                echo '<script>document.getElementById("entretienconseiltemps").textContent="Semestriel";</script>';
            }
            else
            {
                echo '<script>document.getElementById("'.$optionsdisplay[$x].'").textContent="Option retenue";</script>';
            }
        }
    }
    else
    {
        if($optionscheck[$x]=="107" && $entretienconseilok==1)
        {
                
        }
        else
        {
            echo '<script>document.getElementById("'.$optionsdisplay[$x].'").textContent="Non disponible";</script>';
        }
    }
} 