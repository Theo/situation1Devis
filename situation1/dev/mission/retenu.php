<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");
$query="SELECT * from devis where id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $packchoisi=$ligne['packchoisi'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
        span{
            color: #595959 !important;
        }
    }
    span.options {
        width:600px;
        color:#595959; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:61.5%;
        left:10%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12pt;
}
    span.honoraires {
        width:600px;
        color:#595959; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:91.5%;
        left:15%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12pt;
}
    </style>
  </head>
        <!-- page content -->
        <div style="width:1000px;margin-left:10px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <?php
                    if($packchoisi=="1")
                    {
                        echo '<img src="../images/mission/13/retenu1.png" width="100%">';
                    }
                    else if($packchoisi=="2")
                    {
                        echo '<img src="../images/mission/13/retenu2.png" width="100%">';
                    }
                    else
                    {
                        echo '<img src="../images/mission/13/retenu3.png" width="100%">';
                    }
                    ?>
                    <span class="options">
                        <?php 
                            $query="SELECT l.nom as nom,(select count(*) from optionsdevis where devis=o.devis and options!=0) as count FROM `optionsdevis` o inner join listes l on l.id=o.options where o.devis=".$id." and l.option=1";
                            $req=$connexion->query($query);
                            $i=0;
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                $i++;
                                echo "- ".$ligne['nom']."<br/>";
                            }
                            if($i=="0")
                            {
                                echo "Aucune option retenue";
                            }
                        ?> 
                    </span>
                    <span class="honoraires">
                        85€
                    </span>
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://137.74.174.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->