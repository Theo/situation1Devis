<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
    span.imageSignature {
        position:absolute; 
        top:620px;
        left:550px;
        width:50%;
}
    </style>
  </head>
        <!-- page content -->
        <div style="width:1000px;margin-left:10px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <img src="../images/mission/cgv/titreCompta.PNG" width="100%">
                    <img src="../images/mission/cgv/cgvCompta.PNG" width="99%">
                    <span class="imageSignature">
                        <img src="../images/mission/cgv/SignatureSogec.PNG">
                    </span>
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://137.74.174.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->