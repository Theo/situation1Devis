<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");
$query="SELECT (select nom from entreprise where id=d.entreprise) as societe,
(select nom from listes where id=e.statut) as forme,
(select chiffresAffaires from entreprise where id=d.entreprise) as ca,
(select nom from listes where id=e.secteur)  as secteur,
(select dirigeant from entreprise where id=d.entreprise)  as dirigeant,
(select nbAssocies from entreprise where id=d.entreprise)  as nbAssocies,
(select datecloture from infosdevis where devis=d.id)  as datecloture,
(select commissaireComptes from infosdevis where devis=d.id)  as commissaire,
(select nbSalaries from entreprise where id=d.entreprise) as nbSalaries
FROM `devis` d
inner join entreprise e on e.id=d.entreprise
WHERE d.id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $societe=$ligne['societe'];
    $forme=$ligne['forme'];
    $ca=$ligne['ca'];
    $secteur=$ligne['secteur'];
    $dirigeant=$ligne['dirigeant'];
    $nbAssocies=$ligne['nbAssocies'];
    $datecloture=$ligne['datecloture'];
    $commissaire=$ligne['commissaire'];
    $nbSalarie=$ligne['nbSalaries'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>

    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
    }
    span.dirigeant {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:57.7%;
        left:53%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.nbAssocies {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:67.2%;
        left:53%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.nbSalaries {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:70.5%;
        left:7%;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.datecloture {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:77.4%;
        left:53%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
span.commissaire {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:89%;
        left:7%;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
    span.societe {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:51%;
        left:7%;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
    span.forme {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:61%;
        left:7%;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
    span.ca {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:80.2%;
        left:7%;
        text-align:right;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}
    span.secteur {
        width:40%; 
        float:right;
        color:black; 
        background-color:transparent;
        padding:10px; 
        position:absolute; 
        top:86%;
        left:53%;
        text-align:left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13pt;
}span.colorBlue {
    color:blue;
    font-weight: bold;
}
    </style>
  </head>
  <body style='height: 100%;width:100%'>
        <!-- page content -->
        <div style="width:1000px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <img src="../images/mission/3/entreprise.PNG" width="80%"><br/>
                    <!--
                    <span class="dirigeant"><b><?php //echo $dirigeant; ?></b></span>
                    <span class="nbAssocies"><b><?php //if($nbAssocies >=1){ echo $nbAssocies." associés";}else{ echo "Pas d\'associés.";} ?></b></span>
                    <span class="datecloture"><b><?php //echo 'Clôture : '.$datecloture; ?></b></span>
                    <span class="commissaire"><b><?php //if($commissaire=="oui"){echo "Commissaire aux comptes";}else{echo '';}?></b></span>
                    <span class="societe"><b><?php //echo $societe; ?></b></span>
                    <span class="forme"><b><?php //echo $forme; ?></b></span>
                    <span class="nbSalaries"><b><?php //if($nbSalarie >=1){ echo $nbSalarie." salariés";}else{ echo "Pas de salariés.";} ?> </b></span>
                    <span class="ca"><b><?php //echo "CA : ".$ca; ?> €</b></span>
                    <span class="secteur"><b><?php //echo 'Activité : '.$secteur; ?></b></span>
                    <?php// if($commissaire=="oui"){echo '<img src="../images/mission/3/line.PNG">';}else{echo '<img src="../images/mission/3/linesans.PNG">';}?>
                    -->
                    <span class="dirigeant"><?php echo 'Noms des gérants/dirigeants : <span class="colorBlue">'.$dirigeant; ?></b></span></span>
                    <span class="nbAssocies"><?php echo 'Nombre d\'associés  : <span class="colorBlue">'.$nbAssocies; ?></span></span>
                    <span class="datecloture"><?php echo 'Date de clôture : <span class="colorBlue">'.$datecloture; ?></span></span>
                    <span class="ca"><?php echo 'Secteur d\'activité : <span class="colorBlue">'.$secteur; ?></span></span>
                    <span class="societe"><?php echo 'Nom de la société : <span class="colorBlue">'.$societe; ?></span></span>
                    <span class="forme"><?php echo 'Forme juridique : <span class="colorBlue">'.$forme; ?></span></span>
                    <span class="nbSalaries"><?php echo 'Chiffre d\'affaires : <span class="colorBlue">'.$ca.' €'; ?></span></span>
                    <span class="secteur"><?php echo 'Avez-vous un commissaire aux comptes ? : <span class="colorBlue">'.$commissaire; ?></span></span>
                    <img src="../images/mission/3/linesans.PNG">
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://1330.304.1304.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->