<?php
session_start();
include('../init/database.php');
$id=$_GET['id'];

$connexion->query("SET NAMES UTF8");

$connexion->query("SET NAMES UTF8");
$query="SELECT (select nom from entreprise where id=d.entreprise) as societe,
(select dirigeant from entreprise where id=d.entreprise) as nom,
(SELECT distinct(nom) FROM `insee` WHERE insee=e.ville) as ville,
(SELECT postal FROM entreprise WHERE id=d.entreprise) as postal,
(SELECT adresse FROM entreprise WHERE id=d.entreprise) as adresse,
(SELECT nom FROM listes WHERE id=e.statut) as statut
FROM `devis` d
inner join entreprise e on e.id=d.entreprise
WHERE d.id=".$id;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $nom=$ligne['nom'];
    $societe=$ligne['societe'];
    $adresse=$ligne['adresse'];
    $postal=$ligne['postal'];
    $ville=$ligne['ville'];
    $statut=$ligne['statut'];
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @media print  
    {
        div{
            page-break-inside: avoid;
        }
        span {
        width:100%; 
        color:#FFFFFF; 
        background-color:transparent; 
        float:left; 
        padding:10px; 
        position:absolute; 
        bottom:15px; 
        left:10px;
        text-align:left;
        font-size: 13pt
    }
    }
    @media print {
    *,
    *:before,
    *:after {
      background: transparent !important;
      color: #FFFFFF !important;
    }
    span {
        width:100%; 
        color:#FFFFFF; 
        background-color:transparent; 
        float:left; 
        padding:10px; 
        position:absolute; 
        bottom:15px; 
        left:10px;
        text-align:left;
        font-size: 13pt
    }
    </style>
  </head>
        <!-- page content -->
        <div style="width:1000px;margin-left:10px;">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <center>
                    <span class="lol"><b style="color:white;"> Lettre de mission<br/>
                        <?php echo $statut ." ". $societe; ?><br/>
                        <?php echo $nom; ?><br/>
                        <?php echo $adresse; ?><br/>
                        <?php echo $postal." ".$ville; ?>
                    </b><br/>
                    Le <?php echo date('d/m/Y'); ?></span>
                    <img src="../images/mission/sogecbackground.png" width="100%">
                </center>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
</html>


<!-- print via linux: xvfb-run wkhtmltopdf http://137.74.174.15/devis/dev/pdf.php /var/www/devis/dev/pdf.pdf -->