<?php
session_start();
ini_set('error_reporting', E_ALL);
include('include/header.php');
include('init/database.php');
if (isset($_GET['id']))
{
$_GET['id']=$_SESSION['equipe'];
}

?>
          <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Administration <small>Choix d'une vue</small></h3>
              </div>
            </div>
 

    <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Choix d'une vue</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form name="page" data-parsley-validate class="form-horizontal form-label-left" autocomplete="off"  method="POST" action="action.php">
                    <div class="form-group">
                                  <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sélectionnez une vue</label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      
                      <select name="page" class="form-control col-md-7 col-xs-12">
          <?php //  <select name="page" onchange="document.location=this.value"  class="form-control col-md-7 col-xs-12">'; 
?>
                          <option value="-1">-- Veuillez choisir une vue --</option>
 <?php
  $connexion->query("SET NAMES UTF8");
$query="select id, nom from equipe";
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
   echo '<OPTION value="dashboard.php?id='.$ligne['id'].'"> '.$ligne['nom'].'</OPTION>';
}

echo'<OPTION value="dashboardGlobal.php">Vue globale</option>';  

  ?>
      </select>
          </div>
                </div>

                    
                       
                      <input type="hidden" name="equipe" value="<?php echo $_GET['id']; ?> ">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         <input type="reset" class="btn btn-primary" value="Annuler"</input>
                         <input type="submit" class="btn btn-success" value="Valider"</input>
                        </div>
                      </div>
      
           
                      </form>
                                     
                    
                  </div>
                </div>
              </div>
            </div>
         </div>
        </div>
    <?php
    include('include/footer.php');
    ?>