﻿<?php
session_start();
include('init/database.php');
include('include/header.php');
?>

<!-- jQuery -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="js/jquery-steps/jquery.steps.js"></script>
<link href="css/jquery-steps/jquery.steps.css" rel="stylesheet">
<!-- JS file -->
<script src="js/autocomplete/jquery.easy-autocomplete.min.js"></script> 

<!-- CSS file -->
<link rel="stylesheet" href="js/autocomplete/easy-autocomplete.min.css"> 
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Additional CSS Themes file - not required-->
<link rel="stylesheet" href="js/autocomplete/easy-autocomplete.themes.min.css"> 
<style>
    label[disabled]{
        pointer-events:none;
    }
    #divLoading
    {
        display : none;
    }
    #loader
    {
        display : none;
    }
    #divLoading.show
    {
        display : block;
        position : fixed;
        z-index: 100;
        background-color:#666;
        opacity : 0.2;
        background-repeat : no-repeat;
        background-position : center;
        left : 0;
        bottom : 0;
        right : 0;
        top : 0;
    }
    #loader.show{
        display : block;
        position : fixed;
        z-index: 100;
        background-image : url('https://benchmark-hopital.fr/images/loading.gif');
        background-color:transparent;
        opacity : 10;
        background-repeat : no-repeat;
        background-position : center;
        left : 0;
        bottom : 0;
        right : 0;
        top : 0;
    }
    #loadinggif.show
    {
        left : 50%;
        top : 50%;
        position : absolute;
        z-index : 101;
        width : 32px;
        height : 32px;
        margin-left : -16px;
        margin-top : -16px;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <!--
        <div id="loader">
            <div id="divLoading">

            </div>
        </div>-->
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Création de devis</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="example-form" method="POST" action="deviscreate.php" class="form-horizontal form-label-left">
                            <div>
                                <h3>Votre entreprise</h3>
                                <section>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="nom">Nom de la société *
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="nom" name="nom" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Forme juridique *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="forme" id="forme" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une forme juridique --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $queryforme = "select * from listes where type='FJU'";
                                                $reqforme = $connexion->query($queryforme);
                                                while ($ligneforme = $reqforme->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $ligneforme['id'] . '">' . $ligneforme['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dirigeant">Nom et prénom du dirigeant *
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="dirigeant" name="dirigeant"  class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="adresse" class="control-label col-md-3 col-sm-3 col-xs-12">Adresse *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="adresse" name="adresse" class="form-control col-md-7 col-xs-12" type="text" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="postal" class="control-label col-md-3 col-sm-3 col-xs-12">Code Postal *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="postal" class="form-control col-md-7 col-xs-12" onChange='remplirVille()' type="text" name="postal" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ville" class="control-label col-md-3 col-sm-3 col-xs-12">Ville *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="ville" class="form-control col-md-7 col-xs-12" type="text" name="ville" autocomplete="off">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="secteur" class="control-label col-md-3 col-sm-3 col-xs-12">Secteur d'activité *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="secteur" id="secteur" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir un secteur d'activité --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $querysecteur = "select * from listes where type='SEA'";
                                                $reqsecteur = $connexion->query($querysecteur);
                                                while ($lignesecteur = $reqsecteur->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $lignesecteur['id'] . '">' . $lignesecteur['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Activité internationale *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p style='margin-top:5px;'>
                                                Oui:
                                                <input type="radio" class="flat" name="international" id="internationalyes" value="oui" required /> Non:
                                                <input type="radio" class="flat" name="international" id="internationalno" value="non" checked="" />
                                            </p>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="chiffresAffaires" class="control-label col-md-3 col-sm-3 col-xs-12">Chiffres d'affaires *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" placeholder="Montant à saisir en € HT" name="chiffresAffaires" id="chiffresAffaires">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nbDirigeant" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre de gérant(s) / dirigeant(s) *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" name="nbDirigeant" id="nbDirigeant" min="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nbAssocies" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre d'associé(s) *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" name="nbAssocies" id="nbAssocies" min="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nbSalaries" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre de salarié(s) *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" onchange="nbSalarie()" name="nbSalaries" id="nbSalaries" min="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="prospects" class="control-label col-md-3 col-sm-3 col-xs-12">Origine du prospects *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="prospects" id="prospects" class="form-control col-md-7 col-xs-12" onchange="affichageDetail()">
                                                <option value="-1">-- Veuillez choisir l'origine du prospects --</option>
                                                <option value="1">Companeo / fizeo</option>
                                                <option value="2">Interne / développement client</option>
                                                <option value="3">Partenaire</option>
                                                <option value="4">Recommendation</option>
                                                <option value="5">Demande directe</option>
                                                <option value="6">Pub / propection</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="saisieLibre" class="control-label col-md-3 col-sm-3 col-xs-12">Détails *</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" name="saisieLibre" id="saisieLibre" disabled="true">
                                        </div>
                                    </div>
                                    <script>
                                    function affichageDetail()
                                    {
                                        console.log(document.getElementById("prospects").value);
                                        if (document.getElementById("prospects").value=="2" || document.getElementById("prospects").value=="3" || document.getElementById("prospects").value=="4" || document.getElementById("prospects").value=="6")
                                        {
                                            document.getElementById("saisieLibre").disabled=false;
                                        }
                                        else
                                        {
                                            document.getElementById("saisieLibre").disabled=true;
                                            document.getElementById("saisieLibre").value="";
                                        }
                                    }
                                    </script>
                                </section>

                                <h3>Votre comptabilité</h3>
                                <section>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comtpabilité de trésorerie?*</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p style='margin-top:5px;'>
                                                Oui:
                                                <input type="radio" class="flat" name="comptatreso" id="comptatresoyes" value="oui" required /> Non:
                                                <input type="radio" class="flat" name="comptatreso" id="comptatresono" value="non" checked="" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="logiciel">Vous préférez que la saisie comptable soit faite : <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="quisaisie" id="quisaisie" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir qui fait la saisie comptable --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $queryqsa = "select * from listes where type='QSA'";
                                                $reqqsa = $connexion->query($queryqsa);
                                                while ($ligneqsa = $reqqsa->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $ligneqsa['id'] . '">' . $ligneqsa['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="logiciel">Quel est le logiciel de saisie comptable? <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="logiciel" id="logiciel" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir un logiciel --</option>
                                            <?php
                                            $connexion->query("SET NAMES UTF8");
                                            $querylogiciel = "select * from listes where type='LOG'";
                                            $reqlogiciel = $connexion->query($querylogiciel);
                                            while ($lignelogiciel = $reqlogiciel->fetch(PDO::FETCH_ASSOC)) {
                                                echo '<option value="' . $lignelogiciel['id'] . '">' . $lignelogiciel['nom'] . '</option>';
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nbFactureAchat">Estimation du nombre de factures d'achat / mois <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="nbFactureAchat" name="nbFactureAchat" required="required" class="form-control col-md-7 col-xs-12" min='0'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="transmissionfactures">Méthode de transmission des factures <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="transmissionfactures" name='transmissionfactures' required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une méthode de transmission --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $querymethodefacture = "select * from listes where type='MTF'";
                                                $reqmethodefacture = $connexion->query($querymethodefacture);
                                                while ($lignemethodefacture = $reqmethodefacture->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $lignemethodefacture['id'] . '">' . $lignemethodefacture['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nbcategorieachat">Nombre de catégorie d'achat (ventilation analytique) <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="nbcategorieachat" name="nbcategorieachat" placeholder="Nombre de catégories d'achats pouvant se retrouver sur une seule facture métier (exemple métro)" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="caisse">Avez-vous une caisse?<span class="required">*</span>
                                        </label>
                                        <p style='margin-top:5px;'>
                                            Oui:
                                            <input type="radio" class="flat" name="caisse" id="caisseyes" value="oui" required /> Non:
                                            <input type="radio" class="flat" name="caisse" id="caisseno" value="non" checked="" />
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="transmissioncaisse">Méthode de transmission de la caisse <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="transmissioncaisse" name="transmissioncaisse" required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une méthode de transmission --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $querytransmissioncaisse = "select * from listes where type='MCA'";
                                                $reqtransmissioncaisse = $connexion->query($querytransmissioncaisse);
                                                while ($lignetransmissioncaisse = $reqtransmissioncaisse->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $lignetransmissioncaisse['id'] . '">' . $lignetransmissioncaisse['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nbfacturevente">Estimation nombre de facture de vente / mois<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="nbfacturevente" name='nbfacturevente' required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="transmissionfacturesvente">Méthode de transmission des factures de vente <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="transmissionfacturesvente" name="transmissionfacturesvente" required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une méthode de transmission --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $querymethodefacturevente = "select * from listes where type='MFV'";
                                                $reqmethodefacturevente = $connexion->query($querymethodefacturevente);
                                                while ($lignemethodefacturevente = $reqmethodefacturevente->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $lignemethodefacturevente['id'] . '">' . $lignemethodefacturevente['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nbcategorieca">Nombre de catégorie de CA(Ventilation analytique)<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="nbcategorieca" name='nbcategorieca' placeholder="Ventilation du Chiffre d'affaire par catégorie (exemple : service et materiel)" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="transmissionnotefrais">Comment transmettez-vous vos notes de frais <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="transmissionnotefrais" name="transmissionnotefrais" required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une méthode de transmission --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $querymethodenotefrais = "select * from listes where type='MNF'";
                                                $reqmethodenotefrais = $connexion->query($querymethodenotefrais);
                                                while ($lignemethodenotefrais = $reqmethodenotefrais->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $lignemethodenotefrais['id'] . '">' . $lignemethodenotefrais['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="organisation">Qui fait la TVA?<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="quitva" name='quitva' required="required" class="form-control col-md-7 col-xs-12"  onChange="affichage();">
                                                <option value="-1" name="cabinet">Cabinet</option>
                                                <option value="2" name="client">Le client</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="nbtauxtva">Nombre de taux de TVA<span class="required">*</span>

                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="nbtauxtva" min="0" name='nbtauxtva' placeholder="Mettre 0 si client ou non assujetti" required="required" class="form-control col-md-7 col-xs-12">

                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        function affichage() {
                                            var n = document.getElementById('quitva').value;
                                            console.log(n);
                                            if (n == 2)
                                            {
                                                $("#periodicite").val('160');
                                                document.getElementById("nbtauxtva").value = "0";
                                            }
                                            else
                                            {
                                                $("#periodicite").val('-1');
                                                document.getElementById("nbtauxtva").value = "";
                                            }
                                        }
                                    </script>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="periodicite">Périodicité <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="periodicite" name='periodicite' required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une périodicité --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $queryperiodicite = "select * from listes where type='PER'";
                                                $reqperiodicite = $connexion->query($queryperiodicite);
                                                while ($ligneperiodicite = $reqperiodicite->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $ligneperiodicite['id'] . '">' . $ligneperiodicite['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </section>
                                <h3>Social</h3>
                                <section>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bulletinspaie">Qui fait les bulletins de paie <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="bulletinspaie" name="bulletinspaie" required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir qui fait les bulletins de paie --</option>
                                                <option name="cabinet" value="165">Cabinet</option>
                                                <option name="client" value="164">Le client</option>                              


                                            </select>  
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nbbulletins">Nombre de bulletins<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="nbbulletins" name="nbbulletins" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="transmissionvariablespaie" class="control-label col-md-3 col-sm-3 col-xs-12">Méthode de transmission variables de paie</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="transmissionvariablespaie" name="transmissionvariablespaie" required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une méthode de transmission --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $querytransmissionvariablespaie = "select * from listes where type='MTV'";
                                                $reqtransmissionvariablespaie = $connexion->query($querytransmissionvariablespaie);
                                                while ($lignetransmissionvariablespaie = $reqtransmissionvariablespaie->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $lignetransmissionvariablespaie['id'] . '">' . $lignetransmissionvariablespaie['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" id="transmissionvariablesdepaie" name="transmissionvariablesdepaie">
                                        </div>
                                    </div>
                                </section>
                                <h3>Informations complémentaires</h3>
                                <section>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="commissairecomptes">Avez-vous un commissaire aux comptes <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p style='margin-top:5px;'>
                                                Oui:
                                                <input type="radio" class="flat" name="commissairecomptes" id="commissairecomptesyes" value="oui" required /> Non:
                                                <input type="radio" class="flat" name="commissairecomptes" id="commissairecomptesno" value="non" checked="" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="organisation">Qui fait la partie juridique annuelle?<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="juridiqueannuelle" name='juridiqueannuelle' required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">Fulturis</option>
                                                <option value="1">Autre cabinet</option>
                                                <option value="2">Le client</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="datecloture">Date de clôture <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="datecloture" id="datecloture" class="form-control col-md-7 col-xs-12" required="required">
                                                <option value="-1">-- Veuillez choisir une date de clôture --</option>
                                                <option value="31/01">31/01</option>
                                                <option value="28/02">28/02</option>
                                                <option value="31/03">31/03</option>
                                                <option value="30/04">30/04</option>
                                                <option value="31/05">31/05</option>
                                                <option value="30/06">30/06</option>
                                                <option value="31/07">31/07</option>
                                                <option value="31/08">31/08</option>
                                                <option value="30/09">30/09</option>
                                                <option value="31/10">31/10</option>
                                                <option value="30/11">30/11</option>
                                                <option value="31/12">31/12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="budget">Devis / budget actuel <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" id="budget" name='budget' required="required" class="form-control col-md-7 col-xs-12" min='0'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="organisation">Organisation du dossier / client<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="organisation" name='organisation' required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir un niveau d'organisation --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $queryorganisation = "select * from listes where type='ORG'";
                                                $reqorganisation = $connexion->query($queryorganisation);
                                                while ($ligneorganisation = $reqorganisation->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $ligneorganisation['id'] . '">' . $ligneorganisation['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="organisation">Affecter à une équipe comptable<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="equipecompta" name='equipecompta' required="required" class="form-control col-md-7 col-xs-12">
                                                <option value="-1">-- Veuillez choisir une équipe comptable --</option>
                                                <?php
                                                $connexion->query("SET NAMES UTF8");
                                                $queryequipe = "select * from equipe";
                                                $reqequipe = $connexion->query($queryequipe);
                                                while ($ligneequipe = $reqequipe->fetch(PDO::FETCH_ASSOC)) {
                                                    echo '<option value="' . $ligneequipe['id'] . '">' . $ligneequipe['nom'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </section>
                                <div style="text-align: center">
                                    <h4> <a style="color:red" id="displayError"> </a></h4>    
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Devis - créer par <a href="https://applidev.fr">AppliDev'</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<script>



    function nbSalarie() {
        var nb = document.getElementById("nbSalaries").value;
        if (nb < 6)
        {
            var element = document.getElementById('transmissionvariablespaie');
            for (var i = 0; i < element.options.length; i++) {
                if (element.options[i].text === "Mail") {
                    element.selectedIndex = i;
                    document.getElementById("transmissionvariablesdepaie").value = element.value;
                    break;
                }
            }
            document.getElementById("transmissionvariablespaie").disabled = true;
        } else
        {
            var element = document.getElementById('transmissionvariablespaie');
            for (var i = 0; i < element.options.length; i++) {
                if (element.options[i].text === "Navette dématerialisée") {
                    element.selectedIndex = i;
                    document.getElementById("transmissionvariablesdepaie").value = element.value;
                    break;
                }
            }
            document.getElementById("transmissionvariablespaie").disabled = true;
        }
        document.getElementById('nbbulletins').value = nb;
    }
    $(document).ready(function () {

        $("#quisaisie").change(function () {
            if (this.value == "142")
            {
                $("#logiciel").val('145');
                document.getElementsByName('logiciel')[0].options[2].innerHTML = "Non applicable";
                document.getElementById("logiciel").disabled = true;
            } else
            {
                $("#logiciel").val('-1');
                document.getElementsByName('logiciel')[0].options[2].innerHTML = "Utilisation de Sogec Online";
                document.getElementById("logiciel").disabled = false;
            }
        });

/*
        $("input#nbtauxtva").keyup(function () {
            if ((document.getElementById('nbtauxtva').value))
            {
                var n = document.getElementById('nbtauxtva').value;
                console.log(n);
                if (n == 0)
                {
                    $("#periodicite").val('160');
                }
            } else
            {
                console.log('empty');
            }
        });
*/        
        // Fonction si nb salarié=0 alors mettre le cabinet a qui fait la saisie des bulletins
        $("input#nbSalaries").keyup(function () {
            if ((document.getElementById('nbSalaries').value))
            {
                var n = document.getElementById('nbSalaries').value;
                console.log(n);
                if (n == 0)
                {
                    $("#bulletinspaie").val('165');
                }
            } else
            {
                console.log('empty');
            }
        });

        // Foncion si qui fait la tva est à client, mettre dans pédiodicité 0
        /*$("input#quitva").keyup(function () {
            if ((document.getElementById('quitva').value))
            {
                var n = document.getElementById('quitva').value;
                console.log(n);
                if (n == 2)
                {
                    $("#periodicite").val('160');
                }
            } else
            {
                console.log('empty');
            }
        });*/
        
        if (<?php echo $_SESSION["type"]; ?> != 3)
        {
            var element = document.getElementById('equipecompta');
            element.disabled = true;
        }
        disable();
        document.getElementById("caisseno").click();

        $('#caisseyes').on('ifChecked', function () {
            enable();
        })
        $('#caisseno').on('ifChecked', function () {
            disable();
        })
    });

    function disable() {
        var element = document.getElementById('transmissioncaisse');
        element.disabled = true;
        element.value = "-1";
    }
    function enable() {
        document.getElementById("transmissioncaisse").disabled = false;
    }

    var form = $("#example-form");

    jQuery.extend(jQuery.validator.messages, {
        required: "This field is required.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });

    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            var error = 0;
            if (newIndex === 1)
            {
                $("section").find("input").css("background-color", "white");
                $("section").find("select").css("background-color", "white");
                $('#displayError').text("");
                var inputtext = ["nom", "dirigeant", "adresse", "postal"];
                var list = ["forme", "ville", "secteur", "prospects"];
                var nombre = ["chiffresAffaires", "nbDirigeant", "nbAssocies", "nbSalaries"];

                for (var i = 0; i < inputtext.length; i++) {
                    var item = document.getElementById(inputtext[i]);
                    var value = item.value;
                    if (value == null || value == "")
                    {
                        item.style.backgroundColor = "#fc503a";
                        error = 1;
                    }
                }
                for (var i = 0; i < list.length; i++) {
                    var item = document.getElementById(list[i]);
                    var value = item.value;
                    if (value == null || value == "-1" || value == "")
                    {
                        item.style.backgroundColor = "#fc503a";
                        error = 1;
                    }
                }
                for (var i = 0; i < nombre.length; i++) {
                    var item = document.getElementById(nombre[i]);
                    var value = item.value;
                    if (value == null || value == "")
                    {
                        item.style.backgroundColor = "#fc503a";
                        error = 1;
                    }
                }
            }
            if (newIndex === 2)
            {
                $("section").find("input").css("background-color", "white");
                $("section").find("select").css("background-color", "white");
                $('#displayError').text("");
                var list = ["logiciel", "transmissionfactures", "transmissionfacturesvente", "transmissioncaisse", "transmissionnotefrais", "periodicite"];
                var nombre = ["nbFactureAchat", "nbcategorieachat", "nbcategorieca", "nbfacturevente", "nbtauxtva"];

                for (var i = 0; i < list.length; i++) {
                    var item = document.getElementById(list[i]);
                    var value = item.value;
                    if (list[i] == "transmissioncaisse")
                    {
                        if ($('.caisse:checked').val() == "oui")
                        {
                            if (value == null || value == "-1")
                            {
                                item.style.backgroundColor = "#fc503a";
                                error = 1;
                            }
                        } else
                        {
                            //rien
                        }
                    } else
                    {
                        if (value == null || value == "-1")
                        {
                            item.style.backgroundColor = "#fc503a";
                            error = 1;
                        }
                    }
                }
                for (var i = 0; i < nombre.length; i++) {
                    var item = document.getElementById(nombre[i]);
                    var value = item.value;
                    if (value == null || value == "")
                    {
                        item.style.backgroundColor = "#fc503a";
                        error = 1;
                    }
                }
            }
            if (newIndex === 3)
            {
                $("section").find("input").css("background-color", "white");
                $("section").find("select").css("background-color", "white");
                $('#displayError').text("");
                var list = ["bulletinspaie", "transmissionvariablespaie"];
                var nombre = ["nbbulletins"];

                for (var i = 0; i < list.length; i++) {
                    var item = document.getElementById(list[i]);
                    var value = item.value;
                    if (value == null || value == "-1" || value == "")
                    {
                        item.style.backgroundColor = "#fc503a";
                        error = 1;
                    }
                }
                for (var i = 0; i < nombre.length; i++) {
                    var item = document.getElementById(nombre[i]);
                    var value = item.value;
                    if (value == null || value == "")
                    {
                        item.style.backgroundColor = "#fc503a";
                        error = 1;
                    }
                }
            }

            if (error == 1)
            {
                $('#displayError').text("Tous les champs doivent être renseignés!");
                return false;
            }
            $("section").find("input").css("background-color", "white");
            $("section").find("select").css("background-color", "white");
            $('#displayError').text("");
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            //form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            $("section").find("input").css("background-color", "white");
            $("section").find("select").css("background-color", "white");
            $('#displayError').text("");
            if (document.getElementById('equipecompta').disabled) {
                console.log("desactivée");
                var list = ["organisation", "datecloture"];
            }
            else{
                var list = ["organisation", "datecloture", "equipecompta"];
            }
            var nombre = ["budget"];
            var error = 0;
            for (var i = 0; i < list.length; i++) {
                var item = document.getElementById(list[i]);
                var value = item.value;
                if (value == null || value == "-1" || value == "")
                {
                    item.style.backgroundColor = "#fc503a";
                    error = 1;
                }
            }
            for (var i = 0; i < nombre.length; i++) {
                var item = document.getElementById(nombre[i]);
                var value = item.value;
                if (value == null || value == "")
                {
                    item.style.backgroundColor = "#fc503a";
                    error = 1;
                }
            }
            if (error == 1)
            {
                $('#displayError').text("Tous les champs doivent être renseignés!");
                return false;
            }
            $("div#divLoading").addClass('show');
            $("div#loader").addClass('show');
            $("span#loaderspan").addClass('show');
            form.submit();

        },
        /* Labels */
        labels: {
            cancel: "Quitter",
            current: "étape en cours:",
            pagination: "Pagination",
            finish: "Valider",
            next: "Suivant",
            previous: "Précédent",
            loading: "Chargement ..."
        }
    });
</script>
<script>
    var options = {
        url: function (phrase) {
            return "postal?value=" + phrase;
        },
        getValue: "name"
    };

    $("#postal").easyAutocomplete(options);

    function remplirVille() {
        $("#ville").empty();
        var cp = document.getElementById("postal").value;
        var url = "ville.php?cp=" + cp;
        $.getJSON(url, function (return_data) {
            $.each(return_data.data, function (key, value) {
                $("#ville").append("<option value=" + value.insee + ">" + value.nom + "</option>");
            });
        });
    }
</script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>
<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
</body>
</html>