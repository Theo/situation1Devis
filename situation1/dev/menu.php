<?php
session_start();

?>


<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Devis en ligne</title>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.1.min.js"></script>
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="menu.php" class="site_title"><i class="fa fa-pencil-square-o"></i> <span>Devis</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="<?php if ($_SESSION["avoirPhoto"]=="1"){echo 'upload/'.$_SESSION["photo"];}else{echo "images/user.png";} ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenue,</span>
                <h2><?php echo $_SESSION["name"] ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <br/><br/><br/>
                <ul class="nav side-menu">
                  <li><a href='menu.php'><i class="fa fa-home"></i> Menu principal</a>
                  </li>
                  <li><a href='dashboard.php'><i class="fa fa-bar-chart"></i> Tableau de bord</a>
                  </li>
                  <li><a href='devis-list.php'><i class="fa fa-list-ul"></i> Liste de mes devis</a>
                  </li>
                  <li><a href='creationDevis.php'><i class="fa fa-edit"></i> Création de devis</a>
                  </li>
                </ul>
              </div>
                
                <?php
                if($_SESSION["type"]=="1")
                {
                ?>
                    <div class="menu_section">
                      <h3>Administration</h3>
                      <ul class="nav side-menu">
                        <li><a href="utilisateur.php"><i class="fa fa-users"></i>Gestion des utilisateurs</a>
                        </li>
                        <li><a href="equipe.php"><i class="fa fa-user-circle-o"></i>Gestion des équipes</a>
                        </li>
                         <li><a href="objectifequipe.php"><i class="fa fa-user-circle-o"></i>Gestion des objectifs des équipes</a>
                        </li>
                          <li><a href="choixvue.php"><i class="fa fa-user-circle-o"></i>Tableau de bord admin</a></li>
                      </ul>
                    </div>
                <?php
                }
                ?>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php if ($_SESSION["avoirPhoto"]=="1"){echo 'upload/'.$_SESSION["photo"];}else{echo "images/user.png";} ?>?time=<?php echo time();?>" alt=""><?php echo $_SESSION["name"] ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="profile.php"><i class="fa fa-user"></i>  Mon profil</a>
                    <li><a href="login.php"><i class="fa fa-sign-out"></i>  Se déconnecter</a>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->




        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
                <br/><br/><br/><br/><br/><br/><br/>
            </div>
            <div class="row" style='text-align:center'>
                <a href='creationDevis.php' class="btn btn-info btn-lg" style='width:20%'>
                    <i class="fa fa-edit"></i> Créer un devis
                </a>
                <br/>
                <a href='dashboard.php' class="btn btn-primary btn-lg" style='width:20%'>
                    <i class="fa fa-bar-chart"></i> Accéder au tableau de bord
                </a>
                <br/>
                <a href='devis-list.php' class="btn btn-success btn-lg" style='width:20%'>
                    <i class="fa fa-eye"></i> Consulter la liste de mes devis
                </a>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Application Devis - développée par <a href="https://applidev.fr">AppliDev'</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>