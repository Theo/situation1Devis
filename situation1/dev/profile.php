<?php
session_start();
include('init/database.php');
include('include/header.php');
if(isset($_FILES['file'])){
               $modification_photo_ok=0;
               $file_name = $_FILES['image']['name'];
               $file_size =$_FILES['image']['size'];
               $file_tmp =$_FILES['image']['tmp_name'];
               $file_type=$_FILES['image']['type'];
               $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

               $expensions= array("jpeg","jpg","png");

               if(in_array($file_ext,$expensions)=== false){
                  $errors[]="Extension non autorisé, veuillez choisir une image au format JPEG ou PNG.";
               }

               if($file_size > 2097152){
                  $errors[]='La taille du fichier doit être inférieur à 2MB.';
               }

               if(empty($errors)==true){
                  $newfilename = $user_id.'.'.$file_ext;
                  move_uploaded_file($file_tmp, "images/"."$newfilename");
                  $modification_photo_ok=1;
                  $connexion->query("SET NAMES UTF8");
                  $query="update utilisateur set photo=1,linkphoto=".$newfilename." where id=".$_SESSION['user_id'];
                  $req=$connexion->exec($query);
               }else
               {
                   //rien
               }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['cancel'])) {
        //rien
    } else {
        if(isset($_POST['action']))
        {
            $date = date('Y-m-d H:i:s');
            if($_POST['action'] == "modification")
            {
                $connexion->query("SET NAMES UTF8");
                if($_POST['password']!="applidevcgenial")
                {
                    $query="update utilisateur set dateUpdated='".$date."',nom='".$_POST['nom']."',prenom='".$_POST['prenom']."',mail='".$_POST['email']."',username='".$_POST['identifiant']."',password='".sha1($_POST['password'])."',telephone='".$_POST['telephone']."' where id=".$_SESSION['user_id'];
                }
                else
                {
                    $query="update utilisateur set dateUpdated='".$date."',nom='".$_POST['nom']."',prenom='".$_POST['prenom']."',mail='".$_POST['email']."',username='".$_POST['identifiant']."',telephone='".$_POST['telephone']."' where id=".$_SESSION['user_id'];
                }
                if($modification=$connexion->exec($query))
                {
                    $modifUser=1;
                }
                else
                {
                    $modifUser=0;
                }
            }
        }
    }
}
$connexion->query("SET NAMES UTF8");
$query="SELECT nom,prenom,mail,username,telephone,
(select nom from equipe where id=u.equipe) as equipe 
FROM `utilisateur` u
 WHERE id=".$_SESSION['user_id'];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $equipe=$ligne['equipe'];
    $mail=$ligne['mail'];
    $nom=$ligne['nom'];
    $prenom=$ligne['prenom'];
    $username=$ligne['username'];
    $telephone=$ligne['telephone'];
}
?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mon profil</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="<?php if ($_SESSION["avoirPhoto"]=="1"){echo 'upload/'.$_SESSION["photo"];}else{echo "images/user.png";} ?>?time=<?php echo time();?>" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3><?php echo $prenom." ".$nom; ?></h3>

                      <ul class="list-unstyled user_data">

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $equipe; ?>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-envelope user-profile-icon"></i> <?php echo $mail; ?>
                        </li>
                        
                        <li class="m-top-xs">
                          <i class="fa fa-phone user-profile-icon"></i> <?php echo $telephone; ?>
                        </li>
                        
                        <li>
                          <i class="fa fa-user user-profile-icon"></i> Identifiant: <?php echo " ".$username; ?>
                        </li>
                      </ul>

                      <a class="btn btn-success" href="modification-profile.php"><i class="fa fa-edit m-right-xs"></i>Modifier Profil</a>
                      <a class="btn btn-success" href="changer-photo.php"><i class="fa fa-edit m-right-xs"></i>Changer de photo</a>
                      <br />


                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">


                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Derniers devis réalisés</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                            <ul class="messages">
                              <?php
                                $connexion->query("SET NAMES UTF8");
                                $query="SELECT d.id as id,d.dateCreated as date, d.prixdevischoisi as prix, d.packchoisi as pack, (SELECT nom FROM `listes` WHERE id=e.statut) as forme, d.id as iddevis,e.nom as societe, e.dirigeant as gerant, l.nom as secteur,d.prixdevischoisi as prix,d.statut as statut, u.nom as nom, u.prenom as prenom, eq.nom as equipe FROM `devis` d inner join infosdevis i on i.devis=d.id inner join entreprise e on e.id=d.entreprise inner join listes l on l.id=e.secteur inner join utilisateur u on u.id=d.user inner join equipe eq on eq.id=u.equipe where user=".$_SESSION["user_id"]." and d.archive!=1  GROUP BY d.id order by d.dateCreated desc limit 4";
                                $req=$connexion->query($query);
                                while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                                {
                                    $dates = explode(" ", $ligne['date']);
                                    $date=$dates[0];
                                    echo'<li>
                                        <div class="message_date">
                                          <h3 class="date text-info">'; echo day($date); echo'</h3>
                                          <p class="month">'; echo month($date); echo'</p>
                                        </div>
                                        <div class="message_wrapper">
                                          <h4 class="heading">'.$ligne['forme'].' '.$ligne['societe'].'</h4>
                                          <blockquote class="message">';echo $ligne['prix'].' € - '; if($ligne['pack']==1){echo 'Pack Essentiel';}else if($ligne['pack']==2){echo 'Pack Avantage';}else{echo 'Pack Premium';} echo'</blockquote>
                                          <br />
                                          <p class="url">
                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                            <a href="devis/'.$ligne['id'].'/devis.pdf" target="_BLANK"><i class="fa fa-paperclip"></i> devis '.$ligne['forme'].' '.$ligne['societe'].' </a>
                                          </p>
                                        </div>
                                      </li>';
                                }
                              
                              
                              ?>

                            </ul>
                            <!-- end recent activity -->

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

       <!-- footer content -->
        <footer>
          <div class="pull-right">
            Application Devis - développée par <a href="https://applidev.fr">AppliDev'</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- morris.js -->
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <script>
    function  modifySuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'Votre profil a bien été modifié.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
      function modifyError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'La modification de votre profil n\'a pas fonctionné. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
      function  modifyPhotoSuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'Votre photo a bien été modifié.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
      function modifyPhotoError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'La modification de votre photo n\'a pas fonctionné. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
    </script>
  </body>
</html>

<?php

                                    function day ($date){
                                        $dates = explode("-", $date);
                                        $day=$dates[2];
                                        return $day;
                                    }
                                    function month ($date){
                                        $dates = explode("-", $date);
                                        $month=$dates[1]-1;
                                        $montharray=['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre'];
                                        $mois=$montharray[$month];
                                        return $mois;
                                    }
if(isset($modifUser))
{
    if($modifUser == 1)
    {
        echo"<script>modifySuccess()</script>";
    }
    else
    {
        echo"<script>modifyError()</script>";
    }
}
if(isset($modification_photo_ok))
{
    if($modification_photo_ok == 1)
    {
        echo"<script>modifyPhotoSuccess()</script>";
    }
    else
    {
        echo"<script>modifyPhotoError()</script>";
    }
}