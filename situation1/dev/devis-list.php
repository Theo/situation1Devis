<?php
ini_set('error_reporting', E_ALL);
session_start();
include('init/database.php');
include('include/header.php');
if(isset($_GET['action']))
{
    if($_GET['action'] == "delete")
    {
        if(isset($_GET['id']))
        {
            $date = date('Y-m-d H:i:s');
            $connexion->query("SET NAMES UTF8");
            if($archive=$connexion->exec("update devis set archive=1,dateUpdated='".$date."' where id=".$_GET['id']))
            {
                $archiveDevis=1;
            }
            else
            {
                $archiveDevis=0;
            }
        }
    }
}
if(isset($_GET['ok']))
{
    if($_GET['ok'] == "1")
    {
        $modifyDevis=1;
    }
    else
    {
        $modifyDevis=0;
    }
}

?>
        <!-- page content -->
        <div class="right_col" role="main" id="content">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Devis <small>Liste de mes devis</small></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste de mes devis</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Exportez votre liste de devis grâce aux boutons ci-dessous.
                    </p>
                    <table id="datatable-devis" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>N° devis</th>
                          <th>Voir documents</th>
                          <th>Société</th>
                          <th>Nom du gérant</th>
                          <th>Secteur d'activité</th>
                          <th>Origine de prospect</th>
                          <th>Montant du devis</th>
                          <th>Budget actuel</th>
                          <th>Equipe</th>
                          <th>Commercial</th>
                          <th>Statut du devis</th>
                          <th>Récapitulatif</th>
                          <th>Actions</th>
                        </tr>
                      </thead>

                      <tbody>
                          <?php
                          $connexion->query("SET NAMES UTF8");
                          $query="SELECT d.id as iddevis,e.nom as societe, e.dirigeant as gerant, l.nom as secteur,d.prixdevischoisi as prix,d.statut as statut, u.nom as nom, u.prenom as prenom, eq.nom as equipe, d.budgetActuel, i.prospects FROM `devis` d inner join infosdevis i on i.devis=d.id inner join entreprise e on e.id=d.entreprise inner join listes l on l.id=e.secteur inner join utilisateur u on u.id=d.user inner join equipe eq on eq.id=u.equipe where user=".$_SESSION["user_id"]." and d.archive!=1  GROUP BY d.id order by d.id desc";
                          $req=$connexion->query($query);
                          while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                          {
                            echo '<tr>
                            <th>'.$ligne['iddevis'].'</th>
                            <td align="center"><a href="document.php?id='.$ligne['iddevis'].'" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
                            <td>'.$ligne['societe'].'</td>
                            <td>'.$ligne['gerant'].'</td>
                            <td>'.$ligne['secteur'].'</td>
                            <td>'.$ligne['prospects'].'</td>
                            <td>';if($ligne['prix']=='' || $ligne ['prix']==0){echo '0';}else{echo $ligne['prix'];} echo' €</td>
                            <td>'.$ligne['budgetActuel'].'</td>
                            <td>'.$ligne['equipe'].'</td>
                            <td>'.$ligne['nom'].' '.$ligne['prenom'].'</td>
                            <td>'.$ligne['statut'].' <a style="cursor: pointer;" onClick="modifyStatut('.$ligne['iddevis'].')"><i style="font-size:1.5em;" class="fa fa-pencil-square-o"></i></a></td>
                            <td align="center"><a style="cursor:pointer" href="finalisation-devis.php?devis='.$ligne['iddevis'].'"</a><i class="fa fa-list-alt fa-lg" aria-hidden="true"></i></td>
                            <td><span style=" cursor: pointer;" onClick="modify('.$ligne['iddevis'].')"><i style="font-size:1.7em;" class="fa fa-pencil-square-o"></i></span>&nbsp;&nbsp;<span onClick="deleted('.$ligne['iddevis'].')" style=" cursor: pointer;"><i style="font-size:1.7em;" class="fa fa-trash-o"></i></span></td>
                            </tr>';
                          }
                          ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php

include('include/footer.php');
?>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-devis").length) {
            $("#datatable-devis").DataTable({
              dom: "Bfrtip",
              "language": {
                "lengthMenu": "Afficher _MENU_ entrée par page",
                "zeroRecords": "Rien n'a été trouvé - désolé",
                "info": "Affichage de la page _PAGE_ sur _PAGES_",
                "infoEmpty": "Aucunes données disponibles",
                "infoFiltered": "(Filtrage à partir de _MAX_ lignes)",
                "search": "Recherche ",
                "paginate": {
                      "first":      "Premier",
                      "last":       "Dernier",
                      "next":       "Suivant",
                      "previous":   "Précédent"
                      },
                },
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: false
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();
        TableManageButtons.init();
      });
      function resize(){
       var heights = window.innerHeight;
                document.getElementById("content").style.height = heights -50 + "px";
            }
            resize();
            window.onresize = function() {
                resize();
            };
      
                
    function modify(id)
    {
        bootbox.prompt({
            title: "Veuillez choisir l'étape à laquelle vous voulez reprendre la modification.",
            inputType: 'select',
            inputOptions: [
                {
                    text: 'Ajustement du devis',
                    value: '1'
                },
                {
                    text: 'Choix des options, du pack et de la saisie',
                    value: '2'
                }
            ],
            onEscape: function() {
                alert("quit");
            },
            callback: function (result) {
                if(result == null)
                {
                    return;
                }
                choix=result;
                console.log("le choix est:"+choix);
                if(choix==1)
                {
                  document.location.href="ajustement.php?devis="+id+"&modify=1";
                }
                if(choix==2)
                {
                  document.location.href="choix-options.php?devis="+id;
                }
            }
        });
    }
    function modifyStatut(id)
    {
        bootbox.prompt({
            title: "Veuillez choisir le nouveau statut du devis.",
            inputType: 'select',
            inputOptions: [
                {
                    text: 'En Attente',
                    value: 'En Attente'
                },
                {
                    text: 'Validé',
                    value: 'Validé'
                },
                {
                    text: 'Refusé',
                    value: 'Refusé'
                }
            ],
            onEscape: function() {
                alert("quit");
            },
            callback: function (result) {
                if(result == null)
                {
                    return;
                }
                if(result =="Refusé")
                {
                    document.location.href="refusdevis.php?id="+id;
                }
                else
                {
                    document.location.href="updatestatutdevis.php?statut="+result+"&id="+id;
                }
            }
        });
    }
    
    function deleted(id)
    {
        suppressionDevis(id);
    }
    
    function suppressionDevis(id){
        bootbox.confirm('Etes-vous sûr de vouloir supprimer ce devis ?', function(result) {
                if(result==true)
                {
                    var url="devis-list.php?action=delete&id="+id;
                    window.document.location = url;
                }
                else
                {
                    return;
                }
              }); 
    }
    
    function  deleteSuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'Le devis a bien été supprimé.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
    function deleteError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'La suppression du devis ne fonctionne pas. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
      function  modifySuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'Le statut du devis a bien été modifié.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
    function modifyError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'Le statut du devis n\'a pas pu être modifié. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
    </script>
    <!-- /Datatables -->
        <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>

    
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </body>
</html>

<?php
if(isset($archiveDevis))
{
    if($archiveDevis == 1)
    {
        echo"<script>deleteSuccess()</script>";
    }
    else
    {
        echo"<script>deleteError()</script>";
    }
}
if(isset($modifyDevis))
{
    if($modifyDevis == 1)
    {
        echo"<script>modifySuccess()</script>";
    }
    else
    {
        echo"<script>modifyError()</script>";
    }
}
?>