<?php

session_start();
ini_set('error_reporting', E_ALL);
include('init/database.php');
include('include/header.php');
if(isset($_GET['modif']))
{
   
            if($_GET['modif'] == "ok")
            {
                $modifObjectif=1;
            }
            else
            {
                $modifObjectif=0;
            }
        }
    

?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Administration <small>Gestion des objectifs des equipes</small></h3>
              </div>
            </div>
 <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Liste des equipes</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="col-md-4">
                          <p class="text-muted font-13 m-b-30">
                            Exportez vos equipes grâce aux boutons ci-dessous.
                          </p>
                      </div>
                      <div class="col-md-8">
                      </div>
                    <table id="datatable-users" class="table table-striped table-bordered table-responsive">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nom</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                            $connexion->query("SET NAMES UTF8");
                            $query="select * from equipe";
                            $req=$connexion->query($query);
                            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                            {
                                echo'<tr>
                                        <td>'.$ligne['id'].'</td>
                                        <td>'.$ligne['nom'].'</td>
                                        <td><span style=" cursor: pointer;" onClick="modify('.$ligne['id'].')"><i style="font-size:1.7em;" class="fa fa-pencil-square-o"></i></span>&nbsp;&nbsp;</i></span></td>
                                    </tr>';
                            }
                          ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <?php

include('include/footer.php');
?>
    <!-- Datatables -->
   <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-users").length) {
            $("#datatable-users").DataTable({
              dom: "Bfrtip",
              "language": {
                "lengthMenu": "Afficher _MENU_ entrée par page",
                "zeroRecords": "Rien n'a été trouvé - désolé",
                "info": "Affichage de la page _PAGE_ sur _PAGES_",
                "infoEmpty": "Aucune données disponible",
                "infoFiltered": "(Filtrage à partir de _MAX_ lignes)",
                "search": "Recherche ",
                "paginate": {
                      "first":      "Premier",
                      "last":       "Dernier",
                      "next":       "Suivant",
                      "previous":   "Précédent"
                      },
                },
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();
        TableManageButtons.init();
      });
       $('#user tbody').on( 'click', 'span', function () {
                    var data = table.row( $(this).parents('tr') ).data();
                    if(this.id=="modifyTeam")
                    {
                        modify(data[0]);
                    }
                   
                } );
                
    function modify(id)
    {
        document.location.href="modification-objectif-equipe.php?id="+id;
    }
    
    </script>
     <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
      
    <script>
  
      function  modifySuccess(){
          new PNotify({
                    title: 'Succès!',
                    text: 'Les objectifs de l\'équipe ont bien été modifié.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
      }
      function modifyError(){
          new PNotify({
                    title: 'Erreur!',
                    text: 'La modification des objectifs de l\'equipe n\'a pas fonctionné. Veuillez réessayer.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
      }
    </script>
  </body>
  
</html>

<?php

if(isset($modifObjectif))
{
    if($modifObjectif == 1)
    {
        echo"<script>modifySuccess()</script>";
    }
    else
    {
        echo"<script>modifyError()</script>";
    }
}
?>