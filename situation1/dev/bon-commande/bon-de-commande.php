<?php
session_start();
ini_set('error_reporting', E_ALL);
include('../init/database.php');
$date = date('d/m/Y');
$connexion->query("SET NAMES UTF8");
$query="SELECT 
d.saisie as saisie,
(select nom from insee where insee=e.ville group by insee) as ville,
e.adresse as adresse,
e.postal as postal,
d.prixdevischoisi as prix, 
d.packchoisi as pack, 
(SELECT nom FROM `listes` WHERE id=e.statut) as forme, 
e.nom as societe, 
e.dirigeant as gerant,
u.nom as nom
FROM `devis` d 
inner join infosdevis i on i.devis=d.id 
inner join entreprise e on e.id=d.entreprise 
inner join listes l on l.id=e.secteur 
inner join utilisateur u on u.id=d.user 
inner join equipe eq on eq.id=u.equipe 
INNER JOIN fichier f ON f.devis = d.id 
where  d.id=".$_GET['id'];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $societe=$ligne['societe'];
    $adresse=$ligne['adresse'];
    $postal=$ligne['postal'];
    $ville=$ligne['ville'];
    $dirigeant=$ligne['gerant'];
    $forme=$ligne['forme'];
    $prix=$ligne['prix'];
    $saisie=$ligne['saisie'];
    $pack=$ligne['pack'];
}
echo "<script>console.log('".$pack."');</script>";

if($pack=="1")
{
    $query="SELECT SUM( un + remun ) as prix  from devis where id=".$_GET['id'];
    $req=$connexion->query($query);
    while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
    {
        $prixdevis=$ligne['prix'];
    }
}
else if($pack=="2")
{
    $query="SELECT SUM( deux + remdeux ) as prix  from devis where id=".$_GET['id'];
    $req=$connexion->query($query);
    while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
    {
        $prixdevis=$ligne['prix'];
    }
}else if($pack=="3")
{
    $query="SELECT SUM( trois + remtrois ) as prix  from devis where id=".$_GET['id'];
    $req=$connexion->query($query);
    while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
    {
        $prixdevis=$ligne['prix'];
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Devis en ligne</title>
    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md" style="background-color:transparent;">
    <div class="container body">
      <div class="main_container">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-6" style="margin-left:30px;margin-right:30px;margin-top:30px;">
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                                     Bon de commande:
                                     <small class="pull-right"><img src="../images/sg.png" width="200px;"></small>
                                        </br></br>
                                          <small class="pull-right">Date: <?php echo $date; ?></small>
                                      </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Pour
                          <address>
                                          <strong><?php echo $forme." ".$societe ?></strong>
                                          <br><?php echo $dirigeant ?>
                                          <br><?php echo $adresse ?>
                                          <br><?php echo $postal." ".$ville ?>
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Bon de commande #007612</b>
                          <br>
                          <br>
                          <b>N° devis:</b> <?php echo $_GET['id']; ?>
                          <br>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Qte</th>
                                <th>Produit</th>
                                <th style="width: 59%">Description</th>
                                <th style="text-align:center">Sous-total</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td><?php if($pack=="1"){echo 'Pack Essentiel';}else if($pack=="2"){echo 'Pack Avantage';}else if($pack=="3"){echo 'Pack Premium';} ?></td>
                                <td><?php if($pack=="1"){echo 'Pack Essentiel';}else if($pack=="2"){echo 'Pack Avantage';}else if($pack=="3"){echo 'Pack Premium';} if($saisie==1){echo " avec saisie réalisée par le cabinet.";}else{echo " avec saisie réalisée par vos soin.";} ?>
                                </td>
                                <td align="center"><?php echo $prixdevis." €"; ?></td>
                              </tr>
                              <?php
                                $query="SELECT 
                                        (select nom from listes where id=o.options) as nom,
                                        (select prixDeux from pack where idOption=o.options) as prix
                                        FROM `optionsdevis` o
                                        WHERE devis=".$_GET['id'];
                                $req=$connexion->query($query);
                                while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                                {
                                    echo'
                                        <tr>
                                          <td>1</td>
                                          <td>'; echo substr($ligne['nom'], 0, 15); if(strlen($ligne['nom'])>15){echo"..";}  echo'</td>
                                          <td>'.$ligne['nom'].'</td>
                                          <td align="center">'.$ligne['prix'].' €</td>
                                        </tr>';
                                }
                              ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <p class="lead">Recapitulatif</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th>Total:</th>
                                  <td><?php echo $prix." €"; ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                    </section>
                  </div>
                </div>
            </div>
          </div>

      </div>
    </div>

    <!-- jQuery -->
    <script src="../../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../build/js/custom.min.js"></script>
  </body>
</html>