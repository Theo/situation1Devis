<?php
include('init/database.php');
session_start();
$curr_timestamp = date('Y-m-d H:i:s');
$connexion->query("SET NAMES UTF8");
$devis=$_POST['devis'];
$pack=$_POST['pack'];
$saisie=$_POST['saisie'];
$options = explode(",", $_POST['option']);
$length= count($options);
$req="delete from optionsdevis where devis=".$devis;
$delete=$connexion->exec($req);
for($x = 0; $x < $length; $x++) {
    //echo $options[$x];
    if(empty($options[$x]) or $options[$x]=="0")
    {
        
    }
    else
    {
        $req="insert into optionsdevis(devis,options,date) values('$devis','$options[$x]','$curr_timestamp')";
        $insert=$connexion->exec($req);   
    }
}

$query="SELECT sum(prixUn) as un,sum(prixDeux) as deux,sum(prixTrois) as trois FROM `pack` WHERE idOption in (select options from optionsdevis where devis=".$devis.")";
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
   $options1=$ligne['un'];
   $options2=$ligne['deux'];
   $options3=$ligne['trois'];
}

$connexion->query("SET NAMES UTF8");
$query="select * from devis where id=".$devis;
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if($pack=="1")
    {
        $avec=$ligne['un']+$ligne['remun']+$options1;
        $sans=$ligne['quatre']+$ligne['remunsans']+$options1;
        if($saisie=="1")
        {
            $prixdevischoisi=$avec;
        }
        else
        {
            $prixdevischoisi=$sans;
        }
    }
    if($pack=="2")
    {
        $avec=$ligne['deux']+$ligne['remdeux']+$options2;
        $sans=$ligne['cinq']+$ligne['remdeuxsans']+$options2;
        if($saisie=="1")
        {
            $prixdevischoisi=$avec;
        }
        else
        {
            $prixdevischoisi=$sans;
        }                                    
    }
    if($pack=="3")
    {
        $avec=$ligne['trois']+$ligne['remtrois']+$options3;
        $sans=$ligne['six']+$ligne['remtroissans']+$options3;
        if($saisie=="1")
        {
            $prixdevischoisi=$avec;
        }
        else
        {
            $prixdevischoisi=$sans;
        }
    }
}


$reqdevis="update devis set prixdevischoisi='$prixdevischoisi',packchoisi='$pack',saisie='$saisie',dateUpdated='$curr_timestamp' where id=".$devis;
$insert=$connexion->exec($reqdevis);


// VENTILATION DU BUDGET
echo 'Debut ventilation : <br>';

// <editor-fold defaultstate="collapsed" desc="Packchoisis, saisie, remise TABLE devis">
// Recupération packchoisi, saisie et remise
$connexion->query("SET NAMES UTF8");
$query="SELECT packchoisi, saisie, remun, remdeux, remtrois, remunsans, remdeuxsans, remtroissans, budgetActuel FROM devis WHERE id='".$_POST['devis']."'";
echo 'query 1 : '.$query.'<br>';
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $packChoisi=$ligne['packchoisi'];
    $saisie=$ligne['saisie'];
    $budget=$ligne['budgetActuel'];
    if ($packChoisi=='1')
    {
        if ($saisie=='1')
        {
            $remise=$ligne['remun'];
        }
        else if ($saisie=='0')
        {
            $remise=$ligne['remunsans'];
        }
    }
    else if ($packChoisi=='2')
    {
        if ($saisie=='1')
        {
            $remise=$ligne['remdeux'];
        }
        else if ($saisie=='0')
        {
            $remise=$ligne['remdeuxsans'];
        }
    }
    else if ($packChoisi=='3')
    {
        if ($saisie=='1')
        {
            $remise=$ligne['remtrois'];
        }
        else if ($saisie=='0')
        {
            $remise=$ligne['remtroissans'];
        }
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Pourcentage remises ou ajustements">
// Pourcentage des remises
if ($packChoisi=='1')
{
    if ($saisie==='1')
    {
        // Pack1
        $pourcentageBudget=48;
        $pourcentageRevision=17;
        $pourcentageRevisionEC=7;        
        $pourcentageGestion=0;
        $pourcentageSocial=23;
        $pourcentageJuridique=5;
    }
    else if ($saisie=='0')
    {
        // Pack4
        $pourcentageBudget=0;
        $pourcentageRevision=49;
        $pourcentageRevisionEC=15;        
        $pourcentageGestion=0;
        $pourcentageSocial=30;
        $pourcentageJuridique=0;
    }
}
else if ($packChoisi=='2')
{
    if ($saisie==='1')
    {
        // Pack2
        $pourcentageBudget=37;
        $pourcentageRevision=14;
        $pourcentageRevisionEC=6;        
        $pourcentageGestion=14;
        $pourcentageSocial=16;
        $pourcentageJuridique=12;
    }
    else if ($saisie=='0')
    {
        // Pack5
        $pourcentageBudget=0;
        $pourcentageRevision=36;
        $pourcentageRevisionEC=12;        
        $pourcentageGestion=23;
        $pourcentageSocial=21;
        $pourcentageJuridique=9;       
    }
}
else if ($packChoisi=='3')
{
    if ($saisie==='1')
    {
        // Pack3
        $pourcentageBudget=33;
        $pourcentageRevision=11;
        $pourcentageRevisionEC=5;        
        $pourcentageGestion=26;
        $pourcentageSocial=15;
        $pourcentageJuridique=10;       
    }
    else if ($saisie=='0')
    {
        // Pack6
        $pourcentageBudget=0;
        $pourcentageRevision=27;
        $pourcentageRevisionEC=9;        
        $pourcentageGestion=36;
        $pourcentageSocial=17;
        $pourcentageJuridique=11;      
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Juridique, social, gestion, revision, revisionEC TABLE Ventilation">
// Récupération du juridique, social
$connexion->query("SET NAMES UTF8");
$query="SELECT juridique1, juridique2, juridique3, social1, social2, social3, social1sans, social2sans, social3sans, gestion1, gestion2, gestion3, gestion1sans, gestion2sans, gestion3sans, revisionEC1, revisionEC2, revisionEC3, revision1, revision2, revision3 FROM ventilation WHERE idDevis='".$_POST['devis']."'";
echo 'query 2 : '.$query.'<br>';
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if ($packChoisi=='1')
    {
        $juridique=$ligne['juridique1'];
        $revision=$ligne['revision1'];
        $revisionEC=$ligne['revisionEC1'];
        if ($saisie=='1')
        {
            $social=$ligne['social1'];
            $gestion=$ligne['gestion1'];
        }
        else if ($saisie=='0')
        {
            $social=$ligne['social1sans'];
            $gestion=$ligne['gestion1sans'];
        }
    }
    else if ($packChoisi=='2')
    {
        $juridique=$ligne['juridique2'];

        $revision=$ligne['revision2'];
        $revisionEC=$ligne['revisionEC2'];
        if ($saisie=='1')
        {
            $social=$ligne['social2'];
            $gestion=$ligne['gestion2'];
        }
        else if ($saisie=='0')
        {
            $social=$ligne['social2sans'];
            $gestion=$ligne['gestion2sans'];
        }
    }
    else if ($packChoisi=='3')
    {
        $juridique=$ligne['juridique3'];

        $revision=$ligne['revision3'];
        $revisionEC=$ligne['revisionEC3'];
        if ($saisie=='1')
        {
            $social=$ligne['social3'];
            $gestion=$ligne['gestion3'];
        }
        else if ($saisie=='0')
        {
            $social=$ligne['social3sans'];
            $gestion=$ligne['gestion3sans'];
        }
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Options">
// Récupération des options
$tabOptions=array();
$connexion->query("SET NAMES UTF8");
$query="SELECT options FROM optionsdevis WHERE devis ='".$_POST['devis']."'";
echo 'query 3 : '.$query.'<br>';
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    array_push($tabOptions, $ligne['options']);
}
print_r($tabOptions);
echo '<br>';

// Calcul des prix des options
$tabPrixJuridique=array();
$tabPrixSocial=array();
$tabPrixGestion=array();
$tabPrixRevisionEC=array();
$tabPrixRevision=array();
for ($i=0; $i<count($tabOptions); $i++)
{
    // Tableau Juridique
    if ($tabOptions[$i]=='109')
    {
        if ($packChoisi=='1')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query juridique : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixJuridique, $ligne['prixUn']);
            }
        }
    }
    
    if ($tabOptions[$i]=='110')
    {
        if ($packChoisi=='2')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query juridique : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixJuridique, $ligne['prixDeux']);
            }
        }
    } 
    // Tableau Social
    if ($tabOptions[$i]=='97')
    {
        $connexion->query("SET NAMES UTF8");
        $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
        echo 'query social : '.$query.'<br>';
        $req=$connexion->query($query);
        while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
        {
            if ($packChoisi=='1')
            {
                array_push($tabPrixSocial, $ligne['prixUn']);
            }
            else if ($packChoisi=='2')
            {
                array_push($tabPrixSocial, $ligne['prixDeux']);
            }
            else if ($packChoisi=='3')
            {
                array_push($tabPrixSocial, $ligne['prixTrois']);
            }
        }
    }
    
    // Tableau gestion
    if ($tabOptions[$i]=='102' || $tabOptions[$i]=='103')
    {
        if ($packChoisi=='1')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query gestion : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixGestion, $ligne['prixUn']);
            }
        }
    }
    
    if ($tabOptions[$i]=='115' || $tabOptions[$i]=='105')
    {
        if ($packChoisi=='2')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query gestion : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixGestion, $ligne['prixDeux']);
            }
        }
    }
    
    // Tableau RevisionEC
    if ($tabOptions[$i]=='101' || $tabOptions[$i]=='104' || $tabOptions[$i]=='106')
    {
        if ($packChoisi=='1')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query revisionEC : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixRevisionEC, $ligne['prixUn']);
            }
        }
    }
    
    if ($tabOptions[$i]=='107')
    {
        if ($packChoisi=='2')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query revisionEC : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixRevisionEC, $ligne['prixDeux']);
            }
        }
    }
    
    // Tableau revision
    if ($tabOptions[$i]=='100')
    {
        if ($packChoisi=='1')
        {
            $connexion->query("SET NAMES UTF8");
            $query="SELECT prixUn, prixDeux, prixTrois from pack where idOption='".$tabOptions[$i]."'";
            echo 'query revision : '.$query.'<br>';
            $req=$connexion->query($query);
            while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
            {
                array_push($tabPrixRevision, $ligne['prixUn']);
            }
        }
    }    
    
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Affichage des variables">
// Affichage des valeurs
echo 'pack choisis : '.$packChoisi.'<br>';
echo 'saisie : '.$saisie.'<br>';
echo 'remise : '.$remise.'<br>';
echo 'juridique : '.$juridique.'<br>';
echo 'social : '.$social.'<br>';
echo 'gestion : '.$gestion.'<br>';
echo 'revision : '.$revision.'<br>';
echo 'revisionEC : '.$revisionEC.'<br>';
echo 'budget : '.$budget.'<br>';
for ($i=0; $i<count($tabOptions); $i++)
{
    echo 'options '.$i.' : '.$tabOptions[$i].'<br>';
}

// Prix options juridique
$optionsPrixJuridique=0;
for ($i=0; $i<count($tabPrixJuridique); $i++)
{
    echo 'tabPrixJuridique[i] : '.$tabPrixJuridique[$i].'<br>';
    $optionsPrixJuridique=$optionsPrixJuridique+$tabPrixJuridique[$i];
    echo 'optionsPrixJuridique '.$i.' : '.$optionsPrixJuridique.'<br>';
}

// Prix options social
$optionsPrixSocial=0;
for ($i=0; $i<count($tabPrixSocial); $i++)
{
    echo 'tabPrixSocial[i] : '.$tabPrixSocial[$i].'<br>';
    $optionsPrixJuridique=$optionsPrixSocial+$tabPrixSocial[$i];
    echo 'optionsPrixSocial '.$i.' : '.$optionsPrixSocial.'<br>';
}

// Prix options gestion
$optionsPrixGestion=0;
for ($i=0; $i<count($tabPrixGestion); $i++)
{
    echo '$tabPrixGestion[i] : '.$tabPrixGestion[$i].'<br>';
    $optionsPrixGestion=$optionsPrixGestion+$tabPrixGestion[$i];
    echo 'optionsPrixGestion '.$i.' : '.$optionsPrixGestion.'<br>';
}

// Prix options revisionEC
$optionsPrixRevisionEC=0;
for ($i=0; $i<count($tabPrixRevisionEC); $i++)
{
    echo '$tabPrixRevisionEC[i] : '.$tabPrixRevisionEC[$i].'<br>';
    $optionsPrixRevisionEC=$optionsPrixRevisionEC+$tabPrixRevisionEC[$i];
    echo 'optionsPrixRevisionEC '.$i.' : '.$optionsPrixRevisionEC.'<br>';
}

// Prix options revisionEC
$optionsPrixRevision=0;
for ($i=0; $i<count($tabPrixRevision); $i++)
{
    echo '$tabPrixRevision[i] : '.$tabPrixRevision[$i].'<br>';
    $optionsPrixRevision=$optionsPrixRevision+$tabPrixRevision[$i];
    echo 'optionsPrixRevision '.$i.' : '.$optionsPrixRevision.'<br>';
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Calculs">
$totalJuridique=$juridique+($remise*($pourcentageJuridique/100))+$optionsPrixJuridique;
$totalSocial=$social+($remise*($pourcentageSocial/100))+$optionsPrixSocial;
$totalGestion=$gestion+($remise*($pourcentageGestion/100))+$optionsPrixGestion;
$totalRevisionEC=$revisionEC+($remise*($pourcentageRevisionEC/100))+$optionsPrixRevisionEC;
$totalRevision=$revision+($remise*($pourcentageRevision/100))+$optionsPrixRevision;
$totalBudget=$budget+($remise*($pourcentageBudget/100));

$heuresJuridique=$totalJuridique/85;
$heuresSocial=$totalSocial/65;
$heuresGestion=$totalGestion/85;
$heuresRevisionEC=$totalRevisionEC/132;
$heuresRevision=$totalRevision/85;
$heuresBudget=$totalBudget/62;
// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="Calcul budget tenue">
$connexion->query("SET NAMES UTF8");
$query="SELECT * from ventilation where idDevis=".$_POST['devis'];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    if($saisie==1)
    {
        switch ($packChoisi) {
        case 1:
            $budgetTenue=$ligne['saisie1'];
            echo "test1";
            break;
        case 2:
            $budgetTenue=$ligne['saisie2'];
            echo "test2";
            break;
        case 3:
            $budgetTenue=$ligne['saisie3'];
            echo "test3";
            break;
        }
    }
    else
    {
        switch ($packChoisi) {
        case 1:
            $budgetTenue=$ligne['saisie4'];
            break;
        case 2:
            $budgetTenue=$ligne['saisie5'];
            break;
        case 3:
            $budgetTenue=$ligne['saisie6'];
            break;
        }
    }
}
$remiseBudgetTenue=$prixdevischoisi;
echo "<br/>Prix devis total".$prixdevischoisi."<br/>";
$percentbudgettenue=$budgetTenue/$prixdevischoisi;
echo "<br/>Pourcentage budget tenue".$percentbudgettenue."<br/>";
$remisebudgettenue=$remise*$percentbudgettenue;
echo "<br/>Remise budget tenue".$remisebudgettenue."<br/>";
echo "<br/>".$budgetTenue."<br/>";
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Affichage totaux et heures">
echo 'total juridique : '.$totalJuridique.'<br>';
echo 'heures juridique : '.$heuresJuridique.'<br>';
echo 'total social : '.$totalSocial.'<br>';
echo 'heures social : '.$heuresSocial.'<br>';
echo 'total gestion : '.$totalGestion.'<br>';
echo 'heures gestion : '.$heuresGestion.'<br>';
echo 'total revisionEC : '.$totalRevisionEC.'<br>';
echo 'heures revisionEC : '.$heuresRevisionEC.'<br>';
echo 'total revision : '.$totalRevision.'<br>';
echo 'heures revision : '.$heuresRevision.'<br>';
echo 'total budget : '.$totalBudget.'<br>';
echo 'heures budget : '.$heuresBudget.'<br>';
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Requetes SQL">
// Requetes
// Juridique
$reqdevis="update ventilation set totaljuridique='".$totalJuridique."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
$reqdevis="update ventilation set heuresjuridique='".$heuresJuridique."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);

// Social
$reqdevis="update ventilation set totalsocial='".$totalSocial."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
$reqdevis="update ventilation set heuressocial='".$heuresSocial."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);

// Gestion
$reqdevis="update ventilation set totalgestion='".$totalGestion."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
$reqdevis="update ventilation set heuresgestion='".$heuresGestion."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);

// RevisionEC
$reqdevis="update ventilation set totalrevisionEC='".$totalRevisionEC."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
$reqdevis="update ventilation set heuresrevisionEC='".$heuresRevisionEC."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);

// Revision
$reqdevis="update ventilation set totalrevision='".$totalRevision."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
$reqdevis="update ventilation set heuresrevision='".$heuresRevision."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);

// Budget
$reqdevis="update ventilation set totalbudget='".$totalBudget."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
$reqdevis="update ventilation set heuresbudget='".$heuresBudget."' where idDevis='".$_POST['devis']."'";
$insert=$connexion->exec($reqdevis);
// </editor-fold>

header('Location: finalisation-devis.php?devis='.$_POST['devis']);
?>
