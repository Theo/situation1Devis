<?php
session_start();
ini_set('error_reporting', E_ALL);
include('include/header.php');
include('init/database.php');
if(isset($_GET['id']))
{
$connexion->query("SET NAMES UTF8");
$query="select * from utilisateur where id=".$_GET['id'];
$req=$connexion->query($query);
while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
{
    $nom=$ligne['nom'];
    $prenom=$ligne['prenom'];
    $identifiant=$ligne['username'];
    $password=$ligne['password'];
    $mail=$ligne['mail'];
    $actif=$ligne['actif'];
    $profile=$ligne['profile'];
    $equipe=$ligne['equipe'];
    $telephone=$ligne['telephone'];
}
?>
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Administration <small>Gestion  des utilisateurs</small></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Modification de l'utilisateur : <?php echo $nom." ".$prenom;  ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form id="ajout" data-parsley-validate class="form-horizontal form-label-left" autocomplete="off" method='POST' action='utilisateur.php'>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nom">Nom <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nom" name="nom" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $nom; ?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prenom">Prénom <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="prenom" name="prenom" required="required" class="form-control col-md-7 col-xs-12"  value='<?php echo $prenom; ?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="identifiant">Identifiant <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="identifiant" name="identifiant" required="required" class="form-control col-md-7 col-xs-12" value='<?php echo $identifiant; ?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Mot de passe
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="password" id="password" name="password" value="applidevcgenial" class="form-control col-md-7 col-xs-12"   autocomplete="new-password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">E-mail</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="email" class="form-control col-md-7 col-xs-12" type="email" name="email" value='<?php echo $mail; ?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="telephone" class="control-label col-md-3 col-sm-3 col-xs-12">Téléphone</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="telephone" name="telephone" class="form-control col-md-7 col-xs-12" type="tel" value='<?php echo $telephone; ?>' pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Actif</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="">
                            <label>
                              <input type="checkbox" name='actif' id='actif' class="js-switch" <?php if($actif=="on"){echo "checked";}else{}  ?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile">Profil utilisateur *</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                          <select id="profile" name='profile' class="form-control" required>
                            <option value="">-- Choisir le profil utilisateur --</option>
                            <?php
                                $connexion->query("SET NAMES UTF8");
                                $query="select * from profileuser";
                                $req=$connexion->query($query);
                                while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                                {
                                    if($profile==$ligne['id'])
                                    {
                                        $select="selected";
                                    }
                                    else
                                    {
                                        $select="";
                                    }
                                    echo '<option '.$select.' value="'.$ligne['id'].'">'.$ligne['nom'].'</option>';
                                }
                                ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile">Equipe</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          
                          <select id="equipe" name='equipe' class="form-control" required>
                            <option value="">-- Choisir une équipe --</option>
                            <?php
                                $connexion->query("SET NAMES UTF8");
                                $query="select * from equipe";
                                $req=$connexion->query($query);
                                while($ligne =  $req->fetch(PDO::FETCH_ASSOC))
                                {
                                    if($equipe==$ligne['id'])
                                    {
                                        $select="selected";
                                    }
                                    else
                                    {
                                        $select="";
                                    }
                                    echo '<option '.$select.' value="'.$ligne['id'].'">'.$ligne['nom'].'</option>';
                                }
                                ?>
                          </select>
                        </div>
                      </div>
                      <input style='display: none' type="text" id="id" name="id" value='<?php echo $_GET['id']; ?>'>
                      <input style='display: none' type="text" id="action" name="action" value="modification">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <input type="submit" name='cancel' class="btn btn-primary" value="Quitter">
                          <input type="submit" name='submit' class="btn btn-success" value="Valider">
                        </div>
                      </div>

                    </form>
                    
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <?php

include('include/footer.php');
?>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="../vendors/bootbox/bootbox.min.js"></script>
        <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </body>
</html>

<?php
}
else
{
    header('Location: utilisateur.php');
}